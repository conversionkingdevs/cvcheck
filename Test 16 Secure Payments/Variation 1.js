function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
var image = "//placehold.it/300x50";

function image1 (image) {
    if (jQuery('.opt_PaymentImage.opt_Mobile').length == 0){
        jQuery('#cart-control-wrapper .cart-summary').append('<img class="opt_PaymentImage opt_Mobile" src="'+image+'" />');
    }
}

function image2 (image) {
    if (jQuery('.opt_PaymentImage.opt_MobileExpand').length == 0){
        jQuery('#cart-control-wrapper .footer').before('<img class="opt_PaymentImage opt_MobileExpand" src="'+image+'" />');
    }
}

function image3 (image) {
    if (jQuery('.opt_PaymentImage.opt_Desktop').length == 0){
        jQuery('#cart-control-wrapper').append('<img class="opt_PaymentImage opt_Desktop" src="'+image+'" />');
    }
}

defer(function () {
	image1(image);
},'#cart-control-wrapper .cart-summary');
defer(function () {
    image2(image);
},'#cart-control-wrapper .footer');
defer(function () {
	image3(image);
},'#cart-control-wrapper');

// Function for watching changes in Check Area or Summary Area
function addOpenAreaFunction () {
    // Select the node that will be observed for mutations
    var SummaryArea = document.getElementById('cart-control-wrapper');

    // Options for the observer (which mutations to observe)
    var config = { childList: true, subtree: true };

    // Callback function to execute when mutations are observed
    var SummaryAreaCallback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                image1(image);
                image2(image);
                image3(image);
            }
        }
    };

    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

    // Create an observer instance linked to the callback function
    var SummaryAreaObserver = new MutationObserver(SummaryAreaCallback);

    // Start observing the target node for configured mutations
    SummaryAreaObserver.observe(SummaryArea, config);
}

addOpenAreaFunction();