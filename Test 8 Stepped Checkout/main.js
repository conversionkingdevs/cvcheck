

/** Helper Functions */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
    }
    return "";
}

var setCrossDomainCookie = function (name, value, days) {
    var expires;
    if (days >= 0) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" +
            date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/;domain=cvcheck.com;";
};

function deleteCookie(name, path, domain) {
    if (getCookie(name)) {
        document.cookie = name + "=" +
            ((path) ? ";path=" + path : "") +
            ((domain) ? ";domain=" + domain : "") +
            ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0) {
            method();

        } else {
            setTimeout(function () { defer(method, selector); }, 50);
        }
    } else {
        setTimeout(function () { defer(method, selector); }, 50);
    }
}

/** Global variables */

var url = window.location.pathname.split("/");
url = url[url.length - 1];

var specialPages = {
    "Home.aspx": function () {
        var loaded = getCookie('optFirstTime');
        if (loaded != "") {
            if (loaded == "true") {

                setTimeout(function () {
                    if (jQuery("#ctl00_MainContent_ButtonIndividualOrder").length > 0) {
                        window.location.href = '/members/CartSelectChecksPackages.aspx?mode=IndividualCheckOrder';
                    } else {
                        window.location.href = '/members/CartSelectChecksPackages.aspx?mode=EmployerInitial';
                    }
                }, 500);
            }
        }
    },
    "SignupComplete.aspx": function () {
        setCrossDomainCookie('optFirstTime', 'true');
    }
}

/** Custom Script */

function addbreadCrumbs(index) {
    jQuery(".opt-bread-container").remove();

    var beforeElement = ".content-wrapper";
    if(jQuery(".content-wrapper").length == 0){
        beforeElement = ".signup > p:first-child";
        jQuery("body").addClass("opt-signup");
    }

    jQuery(beforeElement).before(`
        <div class="opt-bread-container">
        <div class="opt-section">
            <div class="opt-top">
                <span></span>
            </div>
            <div class="opt-bottom">
                <span>Create Account</span>
            </div>
        </div>
        <div class="opt-section">
            <div class="opt-top">
                <span></span>
            </div>  
            <div class="opt-bottom">
                <span>Choose Checks</span>
            </div>
        </div>
        <div class="opt-section">
            <div class="opt-top">
                <span></span>
            </div>
            <div class="opt-bottom">
                <span>Applicant's Details</span>
            </div>
        </div>
        <div class="opt-section">
            <div class="opt-top">
                <span></span>
            </div>
            <div class="opt-bottom">
                <span>Confirmation</span>
            </div>
        </div>
    </div>
    `);
    for (var i = 0; i < index; i++) {
        jQuery(".opt-bread-container .opt-section:eq(" + i + ")").addClass("opt-completed");
    }
    jQuery(".opt-bread-container .opt-section:eq(" + index + ")").addClass("opt-active");
    jQuery(".opt-section a").click(function (e) {
        e.preventDefault();
        if (jQuery(this).parent().hasClass("opt-completed")) {
            window.location.href = jQuery(this).attr("href");
        }
    });
}

function runTest() {
    // REMOVE LINE BELOW AFTER TESTING
    setCrossDomainCookie('optFirstTime', 'true');
    /*** REMEMEBER *****/



    if (specialPages[url] != undefined) {
        specialPages[url]();
    } else {
        var variation = 0;
        jQuery("body").addClass("opt-8v" + variation);
        var active = 0;
        switch (url) {
            case "getstarted":
            case "IndividualSignup.aspx":
                active = 0;
                break;
            case "CartSelectChecksPackages.aspx":
                deleteCookie("optFirstTime", "/",".cvcheck.com");
                active = 1;
                break;
            case "CartAddApplicant.aspx":
            case "CartChooseOrderType.aspx":
            case "CartRequestUploadForms.aspx":
            case "CartVerifyPersonalDetails.aspx":
            case "CartCheckInformation.aspx":
            case "PersonalDetailEdit.aspx":
                active = 2;
                break;
            case "CartOrderConfirmation.aspx":
            case "CreditCardPayment.aspx":
            case "PaymentSuccess.aspx":
                active = 3;
                break;
            default:
                active = -1;
                break;
        }

        if(active != -1){
            addbreadCrumbs(active);
        }
    }
}


defer(runTest, ".signup, .content-wrapper");
