var image = '//placehold.it/1275x1804';

var html = '<div class="col-md-8 col-md-offset-2 optCont"><div class="optLeft optHalf"><img class="optDesktop" src="'+image+'"><span class="optDesktop"><sub>*</sub>Sample Report</span></div><div class="optRight optHalf"></div></div>';

jQuery('.product-sell').closest('.col-md-8').parent().find('.col-md-8:eq(0)').addClass('optOlFirst').before(html);

// Move elements into optRight
var optRight = jQuery('.optRight'),
    oldItems = jQuery('.optOlFirst');

oldItems.find('> h4').appendTo(optRight);
oldItems.find('> div:eq(1)').find('> span:eq(0), > span:eq(1) > span > span, a.cta-button.cta-button-primary').appendTo(optRight);
oldItems.find('> div:eq(0) > div > ul').appendTo(optRight);

optRight.find('span:eq(0)').after('<div class="optPriceCont"></div>');
optRight.find('span:eq(1),span:eq(2),span:eq(3),span:eq(4)').appendTo('.optPriceCont');
optRight.find('').appendTo('.optPriceCont');
optRight.find('> h4').after('<div class="optMobile"><p>Sample Report</p><img class="optMobile" src="'+image+'"></div>');
