/* CUSTOM CODE */
function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
}
;
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
function html (array) {
    var html = '';
    for (x in array) {
        html += '<div class="item"><img src="'+array[x]+'" /></div>';
    }
    return html;
}
function slider_loaded(){

    var images = [
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/399b5abef717307e2e2be900d5097051_absolute_domestics_trades_services.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/353787cb848a4430764bab1aa0acf618_adecco_hr_recruitment.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/1f22b115102fa554e512f6147647dfa2_alinta_energy_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b266c4d6b6087e052bf69520e8a998a1_austism_association_wa_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/41664c7d81ccb8597e907475289496bc_chep_australia_trans_postal_logistics_warehousing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/eb26250617b9be11bf6c1bdd7061951d_chevron_australia_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d25bdf80adbf402a214c3386e1fa5309_crown_perth_logo_gaming.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7cd08e6660a7405002e05b409a0b287a_curtin_university_edu.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/93b015c40beb3973938572eb6e9203ff_fbaa_finance_insurance.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b462b697918ad7ad0dccbeb65cf5f4b9_fujitsu_manufacturing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ec33bf8f1b9e9b3c84957c73fa01ef0_g8_education_edu.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/430675fdb4c9dc5f6985d2d93e71ece2_holcim_construction.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/f92e046063cd2bbeebbcfeba9a8a2b62_hugo_boss_retail.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/dae9b6976d5216383120f825185b95c9_inpex_mining_resources_energy.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9a9a7b0c181828454bd3ddaa52617558_mater_health_health_medical.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/acaf56f5b6823fb9985e8cd1b6310ed2_max_employment_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/aa98af9caf623d08047764f68700bb92_nine_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ae65b9c22e434b9048f7e0b39122111_programmed_hr_recruitment.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/40574ff75707600a4dea20abc35762b8_relationships_australia_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/0ad643d0955719dcec18fb65fb1cfaab_sa_water_gov.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b0eab314788a857558ca8b0e44116c0f_salvation_army_employment_plus_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/418d13317e76787e0da84158b9c56b7c_seven_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d41f510b567ee687abe2567811fae8b2_warner_bros_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/235effa2b8015aed63412c43b7495b17_westtrac_manufacturing.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/73d77fe3f30589689831e34e6bd66831_youi_finance_insurance.jpg"
    ];

    jQuery(".intro-text").parent().html('<div class="carousel-wrap"></div>');

    jQuery(".carousel-wrap").append('<h2>Companies we work with</h2>');

    jQuery(".carousel-wrap").append('<div class="owl-carousel owl-theme">'+html(images)+'</div>');
    
    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="r-arrow" class="custom-slide-next arrow hidden-xs">');
    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="l-arrow" class="custom-slide-prev arrow hidden-xs">');

    jQuery("<div class='testimonials'></div>").insertAfter(".carousel-wrap");

    jQuery('.testimonials').append("<h2>Here's what some of our customers say about us</h2>");
    jQuery('.testimonials').append('<div class="check-section three-boxes"><div class="col-md-4 col-xs-12"><div class="check"><strong>Easy, fast and reliable</strong><p>Upon a colleague\'s recommendation I found this CV Check service very beneficial. An easy to use online service that kept me updated and results provided in a timely and efficient manner. Highly recommended.</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>This was great</strong><p>Had the results back in 1 day. was quick and easy. Would recommend this to anyone who is looking to get their CV check and will be using this again in the future.</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>Amazing turnaround</strong><p>The turnaround timeframe for the check was less than 24 hours. The process was simple and easy to upload photos for the 100 point check. I could not fault this company.</p></div></div></div>');

    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        nav:true,
        dots: false,
        autoWidth: true,
        items: 4,
        navText: [jQuery('.custom-slide-prev'),jQuery('.custom-slide-next')]
    });
}
if (document.location.pathname == '/') {
  defer(function () {
      getScript('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', slider_loaded);
  }, '.testimonials');
}
