function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function () {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0) {
            method();
            jQuery.fn.adjustHeight = function () {
                var maxHeightFound = 0;
                this.css('min-height', '1px');
                this.each(function () {
                    if (jQuery(this).outerHeight() > maxHeightFound) {
                        maxHeightFound = jQuery(this).outerHeight();
                    }
                });
                this.css('min-height', maxHeightFound);
            };
        } else {
            setTimeout(function () {
                defer(method, selector);
            }, 50);
        }
    } else {
        setTimeout(function () {
            defer(method, selector);
        }, 50);
    }
}

function html(array) {
    var html = '';
    for (x in array) {
        html += '<div class="item"><img src="' + array[x] + '" /></div>';
    }
    return html;
}

function updateHeights() {
    if (jQuery(window).width() < 991) {
        jQuery('.check-section.three-boxes p').css('min-height', '1px');
    } else {
        jQuery('.check-section.three-boxes p').adjustHeight();
    }
}


function mobileSlider() {
    if (jQuery(window).width() < 767) {
        jQuery(".check-section.three-boxes").addClass('owl-carousel');
        jQuery(".check-section.three-boxes .col-md-4").addClass('item');
        jQuery('.three-boxes.owl-carousel').owlCarousel({
            loop: false,
            nav: false,
            dots: true,
            autoWidth: false,
            items: 1
        });
    } else if (jQuery(window).width() >= 767) {
        if (jQuery('.check-section.three-boxes').hasClass('owl-carousel')) {
            jQuery('.three-boxes.owl-carousel').owlCarousel('destroy');
            jQuery(".check-section.three-boxes").removeClass('owl-carousel');
            jQuery(".check-section.three-boxes .col-md-4").removeClass('item');
        }
    }
}

function slider_loaded() {
    var images = [
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/399b5abef717307e2e2be900d5097051_absolute_domestics_trades_services.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/353787cb848a4430764bab1aa0acf618_adecco_hr_recruitment.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/1f22b115102fa554e512f6147647dfa2_alinta_energy_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b266c4d6b6087e052bf69520e8a998a1_austism_association_wa_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/41664c7d81ccb8597e907475289496bc_chep_australia_trans_postal_logistics_warehousing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/eb26250617b9be11bf6c1bdd7061951d_chevron_australia_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d25bdf80adbf402a214c3386e1fa5309_crown_perth_logo_gaming.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7cd08e6660a7405002e05b409a0b287a_curtin_university_edu.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/93b015c40beb3973938572eb6e9203ff_fbaa_finance_insurance.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b462b697918ad7ad0dccbeb65cf5f4b9_fujitsu_manufacturing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ec33bf8f1b9e9b3c84957c73fa01ef0_g8_education_edu.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/430675fdb4c9dc5f6985d2d93e71ece2_holcim_construction.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/f92e046063cd2bbeebbcfeba9a8a2b62_hugo_boss_retail.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/dae9b6976d5216383120f825185b95c9_inpex_mining_resources_energy.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9a9a7b0c181828454bd3ddaa52617558_mater_health_health_medical.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/acaf56f5b6823fb9985e8cd1b6310ed2_max_employment_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/aa98af9caf623d08047764f68700bb92_nine_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ae65b9c22e434b9048f7e0b39122111_programmed_hr_recruitment.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/40574ff75707600a4dea20abc35762b8_relationships_australia_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/0ad643d0955719dcec18fb65fb1cfaab_sa_water_gov.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b0eab314788a857558ca8b0e44116c0f_salvation_army_employment_plus_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/418d13317e76787e0da84158b9c56b7c_seven_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d41f510b567ee687abe2567811fae8b2_warner_bros_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/235effa2b8015aed63412c43b7495b17_westtrac_manufacturing.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/73d77fe3f30589689831e34e6bd66831_youi_finance_insurance.jpg"
    ];
    jQuery("div.product-sell").parent().parent().addClass('before-carousel');
    jQuery('<div class="carousel-wrap"></div>').insertBefore(".before-carousel");
    jQuery(".carousel-wrap").append('<h2>Companies we work with</h2>');
    jQuery(".carousel-wrap").append('<div class="owl-carousel owl-theme">' + html(images) + '</div>');

    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="r-arrow" class="custom-slide-next arrow hidden-xs">');
    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="l-arrow" class="custom-slide-prev arrow hidden-xs">');
    jQuery("<div class='testimonials'></div>").insertAfter(".carousel-wrap");
    jQuery('.testimonials').append("<h2>Here's what some of our customers say about us</h2>");
    jQuery('.testimonials').append('<div class="check-section three-boxes"><div class="col-md-4 col-xs-12"><div class="check"><strong>Hassle free</strong><p>I got my police clearance hassle free from CV Check upon my arrival here in Australia. Their service is quick and straight to the point. I would recommend to everyone who needs a police check asap.</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>Super Fast & Easy!</strong><p>CThis was the first time I used CVCheck because I needed to order a National Police Clearance Check. I didn\'t want to go through the Post Office, fill out loads of forms and waste time trying to ensure the colour of my pen and writing were neat enough for the form, so I decided to Google Police Checks online. These guys came up and it was just a matter of entering my details and clicking submit. Super easy and I\'d highly recommend to anyone needing a police check across Australia! Great platform and will definitely use again!</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>Easy and quick</strong><p>Straight forward process, easy to follow application structure, efficient and quick in reverting. Convenient tracking option, secure, private and reliable way to gain a police check online following simple instructions.</p></div></div></div>');
    jQuery('.owl-carousel').owlCarousel({
        loop: true,
        nav: true,
        dots: false,
        autoWidth: true,
        items: 4,
        navText: [jQuery('.custom-slide-prev'), jQuery('.custom-slide-next')]
    });
    updateHeights();
    mobileSlider();
    jQuery(window).resize(function () {
        updateHeights();
        mobileSlider();
    });
}
if (document.location.pathname == '/national-police-check') {
    defer(function () {
        getScript('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', slider_loaded);
    }, 'div.product-sell');
}