$ = jQuery.noConflict();

console.clear();


function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};

jQuery('head').append(`<style>

.owl-theme .owl-dots,.owl-theme .owl-nav{text-align:center;-webkit-tap-highlight-color:transparent}.owl-theme .owl-nav{margin-top:10px}.owl-theme .owl-nav [class*=owl-]{color:#FFF;font-size:14px;margin:5px;padding:4px 7px;background:#D6D6D6;display:inline-block;cursor:pointer;border-radius:3px}.owl-theme .owl-nav [class*=owl-]:hover{background:#869791;color:#FFF;text-decoration:none}.owl-theme .owl-nav .disabled{opacity:.5;cursor:default}.owl-theme .owl-nav.disabled+.owl-dots{margin-top:10px}.owl-theme .owl-dots .owl-dot{display:inline-block;zoom:1}.owl-theme .owl-dots .owl-dot span{width:10px;height:10px;margin:5px 7px;background:#D6D6D6;display:block;-webkit-backface-visibility:visible;transition:opacity .2s ease;border-radius:30px}.owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span{background:#869791}

.owl-carousel,.owl-carousel .owl-item{-webkit-tap-highlight-color:transparent;position:relative}.owl-carousel{display:none;width:100%;z-index:1}.owl-carousel .owl-stage{position:relative;-ms-touch-action:pan-Y;touch-action:manipulation;-moz-backface-visibility:hidden}.owl-carousel .owl-stage:after{content:".";display:block;clear:both;visibility:hidden;line-height:0;height:0}.owl-carousel .owl-stage-outer{position:relative;overflow:hidden;-webkit-transform:translate3d(0,0,0)}.owl-carousel .owl-item,.owl-carousel .owl-wrapper{-webkit-backface-visibility:hidden;-moz-backface-visibility:hidden;-ms-backface-visibility:hidden;-webkit-transform:translate3d(0,0,0);-moz-transform:translate3d(0,0,0);-ms-transform:translate3d(0,0,0)}.owl-carousel .owl-item{min-height:1px;float:left;-webkit-backface-visibility:hidden;-webkit-touch-callout:none}.owl-carousel .owl-item img{display:block;width:100%}.owl-carousel .owl-dots.disabled,.owl-carousel .owl-nav.disabled{display:none}.no-js .owl-carousel,.owl-carousel.owl-loaded{display:block}.owl-carousel .owl-dot,.owl-carousel .owl-nav .owl-next,.owl-carousel .owl-nav .owl-prev{cursor:pointer;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel .owl-nav button.owl-next,.owl-carousel .owl-nav button.owl-prev,.owl-carousel button.owl-dot{background:0 0;color:inherit;border:none;padding:0!important;font:inherit}.owl-carousel.owl-loading{opacity:0;display:block}.owl-carousel.owl-hidden{opacity:0}.owl-carousel.owl-refresh .owl-item{visibility:hidden}.owl-carousel.owl-drag .owl-item{-ms-touch-action:pan-y;touch-action:pan-y;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.owl-carousel.owl-grab{cursor:move;cursor:grab}.owl-carousel.owl-rtl{direction:rtl}.owl-carousel.owl-rtl .owl-item{float:right}.owl-carousel .animated{animation-duration:1s;animation-fill-mode:both}.owl-carousel .owl-animated-in{z-index:0}.owl-carousel .owl-animated-out{z-index:1}.owl-carousel .fadeOut{animation-name:fadeOut}@keyframes fadeOut{0%{opacity:1}100%{opacity:0}}.owl-height{transition:height .5s ease-in-out}.owl-carousel .owl-item .owl-lazy{opacity:0;transition:opacity .4s ease}.owl-carousel .owl-item .owl-lazy:not([src]),.owl-carousel .owl-item .owl-lazy[src^=""]{max-height:0}.owl-carousel .owl-item img.owl-lazy{transform-style:preserve-3d}.owl-carousel .owl-video-wrapper{position:relative;height:100%;background:#000}.owl-carousel .owl-video-play-icon{position:absolute;height:80px;width:80px;left:50%;top:50%;margin-left:-40px;margin-top:-40px;cursor:pointer;z-index:1;-webkit-backface-visibility:hidden;transition:transform .1s ease}.owl-carousel .owl-video-play-icon:hover{-ms-transform:scale(1.3,1.3);transform:scale(1.3,1.3)}.owl-carousel .owl-video-playing .owl-video-play-icon,.owl-carousel .owl-video-playing .owl-video-tn{display:none}.owl-carousel .owl-video-tn{opacity:0;height:100%;background-position:center center;background-repeat:no-repeat;background-size:contain;transition:opacity .4s ease}.owl-carousel .owl-video-frame{position:relative;z-index:1;height:100%;width:100%}

.owl-item {
    width: auto;
    height: 75px;
    display: flex;
    align-items: center;

}
.item {margin:0 10px; }
.item img {display: block;max-width: 150px;max-height: 75px;width: initial !important;;}

.before-carousel {display:block !important;}
.owl-carousel .owl-stage-outer {height: 120px;}
.carousel-wrap {position:relative; padding:0 30px 60px 30px; }
.carousel-wrap .owl-carousel {    max-width: 660px; margin: 0 auto;}

.carousel-wrap:before {content: ""; display: block; position: absolute; height: 100%; background: #fff; width: 4000px; left: -2000px; top: 0;}

.carousel-wrap h2,
.testimonials h2 {font-size:22px; line-height:24px; font-style:italic; position:relative; z-index:100; font-weight:normal; padding:30px 0;}

.carousel-wrap #l-arrow {top:-15px; left:-430px;}
.carousel-wrap #r-arrow {top:-15px; right:-430px;}

.testimonials {margin-bottom:30px;}
.testimonials .check-section.three-boxes {width:calc(100% + 30px); margin-left:-15px;}
.testimonials .check-section.three-boxes .col-md-4 {padding:0 !important;}
.testimonials .check-section.three-boxes .col-md-4 .check {padding:20px; text-align:left;}
.testimonials .check-section.three-boxes .col-md-4 .check p {text-align:left; }
.testimonials .check-section.three-boxes .col-md-4 .check strong {padding:7px 0 10px 0; display:block;}

@media screen and (max-width:1600px){
    .carousel-wrap #l-arrow {top:-15px; left:-290px;}
    .carousel-wrap #r-arrow {top:-15px; right:-290px;}
}

@media screen and (max-width:1300px){
    .carousel-wrap #l-arrow {top:-15px; left:-60px;}
    .carousel-wrap #r-arrow {top:-15px; right:-60px;}
}

@media only screen and (max-width: 1200px){

    .testimonials .check-section.three-boxes .col-md-4 .check {margin-left:5px; margin-right:5px; }

    #police-check-content > div {overflow:hidden;}
}

@media only screen and (max-width: 991px){
    .testimonials .check-section.three-boxes .col-md-4 .check {min-height:1px;}
    .carousel-wrap h2,
    .testimonials h2 {font-size:18px; padding:15px 0;}

    .carousel-wrap {padding-bottom:0;}
}



</style>`);



getScript('https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', slider_loaded);

function html (array) {
    var html = '';
    for (x in array) {
        html += '<div class="item"><img src="'+array[x]+'" /></div>';
    }
    return html;
}

function slider_loaded(){

    var images = [
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/399b5abef717307e2e2be900d5097051_absolute_domestics_trades_services.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/353787cb848a4430764bab1aa0acf618_adecco_hr_recruitment.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/1f22b115102fa554e512f6147647dfa2_alinta_energy_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b266c4d6b6087e052bf69520e8a998a1_austism_association_wa_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/41664c7d81ccb8597e907475289496bc_chep_australia_trans_postal_logistics_warehousing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/eb26250617b9be11bf6c1bdd7061951d_chevron_australia_mining_resources_energy.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d25bdf80adbf402a214c3386e1fa5309_crown_perth_logo_gaming.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7cd08e6660a7405002e05b409a0b287a_curtin_university_edu.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/93b015c40beb3973938572eb6e9203ff_fbaa_finance_insurance.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b462b697918ad7ad0dccbeb65cf5f4b9_fujitsu_manufacturing.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ec33bf8f1b9e9b3c84957c73fa01ef0_g8_education_edu.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/430675fdb4c9dc5f6985d2d93e71ece2_holcim_construction.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/f92e046063cd2bbeebbcfeba9a8a2b62_hugo_boss_retail.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/dae9b6976d5216383120f825185b95c9_inpex_mining_resources_energy.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9a9a7b0c181828454bd3ddaa52617558_mater_health_health_medical.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/acaf56f5b6823fb9985e8cd1b6310ed2_max_employment_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/aa98af9caf623d08047764f68700bb92_nine_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7ae65b9c22e434b9048f7e0b39122111_programmed_hr_recruitment.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/40574ff75707600a4dea20abc35762b8_relationships_australia_nfp.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/0ad643d0955719dcec18fb65fb1cfaab_sa_water_gov.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/b0eab314788a857558ca8b0e44116c0f_salvation_army_employment_plus_jobactive.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/418d13317e76787e0da84158b9c56b7c_seven_network_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/d41f510b567ee687abe2567811fae8b2_warner_bros_media.png",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/235effa2b8015aed63412c43b7495b17_westtrac_manufacturing.jpg",
        "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/73d77fe3f30589689831e34e6bd66831_youi_finance_insurance.jpg"
    ]

    jQuery(".intro-text").parent().html('<div class="carousel-wrap"></div>');

    jQuery(".carousel-wrap").append('<h2>Companies we work with</h2>')

    jQuery(".carousel-wrap").append('<div class="owl-carousel owl-theme">'+html(images)+'</div>')
    
    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="r-arrow" class="custom-slide-next arrow hidden-xs">');
    jQuery('.carousel-wrap').append('<img src="/ResourcePackages/bootstrap/assets/dist/images/arrow.png" id="l-arrow" class="custom-slide-prev arrow hidden-xs">');

    jQuery("<div class='testimonials'/>").insertAfter(".carousel-wrap");

    jQuery('.testimonials').append("<h2>Here's what some of our customers say about us</h2>");
    jQuery('.testimonials').append('<div class="check-section three-boxes"><div class="col-md-4 col-xs-12"><div class="check"><strong>Easy, fast and reliable</strong><p>Upon a colleague\'s recommendation I found this CV Check service very beneficial. An easy to use online service that kept me updated and results provided in a timely and efficient manner. Highly recommended.</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>This was great</strong><p>Had the results back in 1 day. was quick and easy. Would recommend this to anyone who is looking to get their CV check and will be using this again in the future.</p></div></div><div class="col-md-4 col-xs-12"><div class="check"><strong>Amazing turnaround</strong><p>The turnaround timeframe for the check was less than 24 hours. The process was simple and easy to upload photos for the 100 point check. I could not fault this company.</p></div></div></div>');

    jQuery('.owl-carousel').owlCarousel({
        loop:true,
        nav:true,
        dots: false,
        autoWidth: true,
        items: 4,
        navText: [jQuery('.custom-slide-prev'),jQuery('.custom-slide-next')]
    })

}


