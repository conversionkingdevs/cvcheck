$ = jQuery.noConflict();

console.clear();

// $('span:contains("Order Online Now")').next().next().next().next().next().remove();
// $('span:contains("Order Online Now")').next().next().next().next().remove();
// $('span:contains("Order Online Now")').next().next().next().remove();
// $('span:contains("Order Online Now")').next().next().remove();
// $('span:contains("Order Online Now")').next().remove();
// $('span:contains("Order Online Now")').remove();

$(window).scroll(function(){
    if($(window).scrollTop() > jQuery('header').height()){
        $('header').addClass('scrolled');
    } else {
        $('header').removeClass('scrolled');
    }
})

$('<div class="header-police-check" />').insertAfter($('header .mobile-menu'));

$(".header-police-check").append('<h2>Order Online Now</h2>')
$(".header-police-check").append('<h3>National Police Check Price: $49.50 <small>incl GST</small></h3>');
$(".header-police-check").append('<a class="cta-button cta-button-primary" href="http://ww1.cvcheck.com/public/SignUpNow.aspx">Order now</a>');


$('head').append(`<style>
header.scrolled nav {display:none;}
header.scrolled .header-btn {display:none;}
header.scrolled .mobile-menu {display:none;}


.header-police-check {float:right; padding-top:17px; display:none;}
header.scrolled .header-police-check {display:block;}
.header-police-check h2 {font-size:20px; font-weight:700; line-height:20px; display:inline; padding-right:36px;}
.header-police-check h3 {font-size:20px; font-weight:500; line-height:20px; color:#546971; display:inline; padding-bottom:0; padding-right:30px;}
.header-police-check h3 small {font-size:14px;}

@media screen and (max-width:1200px){
    .header-police-check h2, .header-police-check h3 {font-size:16px;}
    .header-police-check h3 small {font-size:12px;}
}

@media screen and (max-width:960px){
    .header-police-check {float:left; padding-left:30px; padding-top:8px;}
    .header-police-check h2 {display:block; text-align:left; font-size:13px; line-height:15px; margin:0;}
    .header-police-check h3 {display:block;  text-align:left; font-size:10px; margin:0;}
    .header-police-check h3 small {font-size:10px;}
    .header-police-check .cta-button {margin-left:0; padding:2px 10px;}
    
}

@media screen and (max-width:600px){
    header .header-logo {width:50px; /* Replace logo with the one in the folder. It didn't work from imgur */}
}
</style>`);