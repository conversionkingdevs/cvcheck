function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
window.optLastName = {
    name:false,
    price:false
};
function compareOrderSummaryName (name, callback) { // Checks names against eachother and if found callsback
    jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class]').each(function () {
        if (name == jQuery(this).find('.dxgv:eq(1)').text().trim()) {
            callback();
        }
    });
}
function addAddSelectedClasses() { // Will add the selected class to the add to cart buttons
    jQuery('.opt_Selected').each(function () {
      jQuery(this).find('.btn').contents().first()[0].textContent='ADD TO CART';
      jQuery(this).removeClass('opt_Selected');
    });
      
    jQuery('.buy-check').each(function () {
        var name = jQuery(this).find('.check-name').text().trim(),
            that = this;
        compareOrderSummaryName(name, function () {
			jQuery(that).find('.btn').contents().first()[0].textContent='ADDED TO CART';
            jQuery(that).addClass('opt_Selected');
        });
    });
}
function addRemoveAddedContainer() {
    jQuery('.opt_AddedContainer').remove();
}
function addAddedToCartHTML() {
    if (jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length > window.CurrentListSize) {
        if (optLastName.name !== false) {
        window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length;
            addRemoveAddedContainer();
            var html = '<div class="opt_AddedContainer"> <p>'+optLastName.name+' '+optLastName.price+' has been added to cart</p><div class="opt_Close"><p>X</p></div></div>';
            console.log(CurrentListSize);
            jQuery('.content-wrapper.container').prepend(html);
            jQuery('.opt_Close').click(function () {
                addRemoveAddedContainer();
            });
        }
    }
}
function addAddtoCartFunctionality() {
    jQuery('.btn.btn-primary.btn-sm').each(function () {
        if (!jQuery(this).hasClass('opt_AddCartFunctionality')) {
            jQuery(this).click(function () {
                // Add the name to check against
                var name = jQuery(this).closest('.buy-check').find('.check-name').text().trim(),
                    price = jQuery(this).closest('.buy-check').find('.check-price li:eq(1)').text().trim().split('incl')[0].trim();
                window.optLastName.name = name;
                window.optLastName.price = price;
                window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length;
            });
        }
    }).addClass('opt_AddCartFunctionality');
}
function addChangedSummaryOrCheckArea () {
    addAddSelectedClasses();
    addAddtoCartFunctionality();
}
// Function for watching changes in Check Area or Summary Area
function addOpenAreaFunction () {
    // Select the node that will be observed for mutations
    var CheckArea = document.getElementById('ASPxPageControl1_C1'),
        SummaryArea = document.getElementById('cart-control-wrapper');
    // Options for the observer (which mutations to observe)
    var config = { childList: true, subtree: true };
    // Callback function to execute when mutations are observed
  
    var CheckAreaCallback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                addChangedSummaryOrCheckArea();
            }
        }
    };
    var SummaryAreaCallback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
              setTimeout(function () {
              	addChangedSummaryOrCheckArea();
                addAddedToCartHTML();
              },100);
            }
        }
    };
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    // Create an observer instance linked to the callback function
    var CheckAreaObserver = new MutationObserver(CheckAreaCallback);
    var SummaryAreaObserver = new MutationObserver(SummaryAreaCallback);
    // Start observing the target node for configured mutations
    CheckAreaObserver.observe(CheckArea, config);
    SummaryAreaObserver.observe(SummaryArea, config);
}
defer(function () {
  	window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length;
	console.log(CurrentListSize);
    addOpenAreaFunction ();
}, '#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr');
