function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
window.optLastName = {
    name:"",
    price:""
};
function compareOrderSummaryName (name, callback) { // Checks names against eachother and if found callsback
    jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class]').each(function () {
        if (name == jQuery(this).find('.dxgv:eq(1)').text().trim()) {
            callback();
        }
    });
}
function addAddSelectedClasses() { // Will add the selected class to the add to cart buttons
    jQuery('#optBasicStyle').remove();
	jQuery('.opt_Selected').each(function () {
      jQuery(this).find('.btn').contents().first()[0].textContent='ADD TO CART';
      jQuery(this).removeClass('opt_Selected');
    });
	jQuery('.opt_Selected').removeClass('opt_Selected');
    jQuery('.buy-check').each(function () {
        var name = jQuery(this).find('.check-name').text().trim(),
            that = this;
        compareOrderSummaryName(name, function () {
            jQuery(that).addClass('opt_Selected');
			jQuery(that).find('.btn').contents().first()[0].textContent='ADDED TO CART';
        });
    })
    var value = CurrentListSize - 1;
    jQuery('body').append('<style id="optBasicStyle">.fa-shopping-cart:after{content: "' + value + '";}</style>');
    console.log(CurrentListSize);
}
function addRemoveAddedContainer() {
    jQuery('.opt_AddedContainer').remove();
}
function addAddedToCartHTML() {
    if (jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length > window.CurrentListSize) {
        window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length
        addRemoveAddedContainer();
        var html = '<div class="opt_AddedContainer"> <p>'+optLastName.name+' '+optLastName.price+' has been added to cart</p><div class="opt_Close"><p>X</p></div></div>'
        jQuery('.content-wrapper.container').prepend(html);
        jQuery('.opt_Close').click(function () {
            addRemoveAddedContainer();
        })
    }
}
function addAddtoCartFunctionality() {
    jQuery('.btn.btn-primary.btn-sm').each(function () {
        if (!jQuery(this).hasClass('opt_AddCartFunctionality')) {
            jQuery(this).click(function () {
                // Add the name to check against
                var name = jQuery(this).closest('.buy-check').find('.check-name').text().trim(),
                    price = jQuery(this).closest('.buy-check').find('.check-price li:eq(1)').text().trim().split('incl')[0].trim();
                window.optLastName.name = name;
                window.optLastName.price = price;
                window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length
            });
        }
    }).addClass('opt_AddCartFunctionality')
}
function addChangedSummaryOrCheckArea () {
    addAddSelectedClasses();
    addAddtoCartFunctionality();
}
// Function for watching changes in Check Area or Summary Area
function addOpenAreaFunction () {
    // Select the node that will be observed for mutations
    var CheckArea = document.getElementById('ASPxPageControl1_C1'),
        SummaryArea = document.getElementById('cart-control-wrapper');
    // Options for the observer (which mutations to observe)
    var config = { childList: true, subtree: true };
    // Callback function to execute when mutations are observed
    var CheckAreaCallback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                addChangedSummaryOrCheckArea();
              setTimeout(function () {
              window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
              window.opta = jQuery('.order-processing').height();
              window.optb = jQuery('.order-processing').offset().top;
              window.optoff = opta-optsummaryHeight+optb;
              jQuery('#optBasicStyle').remove();
                var value = CurrentListSize - 1;
                jQuery('body').append('<style id="optBasicStyle">.fa-shopping-cart:after{content: "' + value + '";}</style>');
                },50)
            }
        }
    };
    var SummaryAreaCallback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                addChangedSummaryOrCheckArea();
                addAddedToCartHTML();
              setTimeout(function () {
              window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
              window.opta = jQuery('.order-processing').height();
              window.optb = jQuery('.order-processing').offset().top;
              window.optoff = opta-optsummaryHeight+optb;
                jQuery('#optBasicStyle').remove();
                var value = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length - 1;
                jQuery('body').append('<style id="optBasicStyle">.fa-shopping-cart:after{content: "' + value + '";}</style>');
                },50)
            }
        }
    };
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    // Create an observer instance linked to the callback function
    var CheckAreaObserver = new MutationObserver(CheckAreaCallback);
    var SummaryAreaObserver = new MutationObserver(SummaryAreaCallback);
    // Start observing the target node for configured mutations
    CheckAreaObserver.observe(CheckArea, config);
    SummaryAreaObserver.observe(SummaryArea, config);
}
defer(function () {
    jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ShoppingCartSteps > .footer').clone().addClass('optnewfooter').appendTo(jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ShoppingCartSteps'));
  window.CurrentListSize = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr:not(#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXEmptyRow)').length
  addOpenAreaFunction ();
  window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
  window.opta = jQuery('.order-processing').height();
  window.optb = jQuery('.order-processing').offset().top;
  window.optoff = opta-optsummaryHeight+optb;
    jQuery('#optBasicStyle').remove();
    var value = CurrentListSize - 1;
    jQuery('body').append('<style id="optBasicStyle">.fa-shopping-cart:after{content: "' + value + '";}</style>');
    console.log(CurrentListSize);
  function offset () {
	if (jQuery(window).width() > 768) {
    var scroll = jQuery(window).scrollTop();
    if (scroll <= optb + 20) {
      jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
        "position": 'initial',
        "top": 'initial',
        "right": 'initial',
        "margin-top": "initial"
      });
    }else if (scroll > optb + 20 && optoff > scroll) {
      jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
        "position": 'absolute',
        "top": scroll - optb,
        "right": '0px',
        "margin-top": "20px"
      });
      if (jQuery(window).width() < 1080 && jQuery(window).width() > 1025) {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
          "right":"20px"
        });
      } else {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
          "right":"0px"
        });
      }
    } else if (scroll < optb + 20 && scroll < optoff) {
      jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
        "position": 'absolute',
        "top": opta-optsummaryHeight,
        "right": '0px',
        "margin-top": "20px"
      });
      if (jQuery(window).width() < 1080 && jQuery(window).width() > 768) {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
          "right":"20px"
        });
      } else {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
          "right":"0px"
        });
      }
    }
    } else {
      jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
        "position": 'initial',
        "top": 'initial',
        "right": 'initial',
        "margin-top": "initial"
      });
    }
  }
  jQuery(window).resize(function () {
    window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
    window.opta = jQuery('.order-processing').height();
    window.optb = jQuery('.order-processing').offset().top;
    window.optoff = opta-optsummaryHeight+optb;
  })
  jQuery(window).scroll(function () {
    offset();
  })
}, '#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable > tbody > tr');
