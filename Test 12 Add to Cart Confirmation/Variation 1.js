window.optLastName = {
    name:"",
    price:""
};
window.optCheckerLimit = 0;

function compareOrderSummaryName (name, callback) {
    jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class]').each(function () {
        if (name == jQuery(this).find('.dxgv:eq(1)').text().trim()) {
            callback();
        }
    });
}

function addAddSelectedClasses() {
        jQuery('.opt_Selected').removeClass('opt_Selected');
        jQuery('.buy-check').each(function () {
            var name = jQuery(this).find('.check-name').text().trim(),
                that = this;
            compareOrderSummaryName(name, function () {
                jQuery(that).addClass('opt_Selected');
            });
        })
}

function addCheckOrderSummary(callback) {
    window.optCheckerLimit++;
    if(jQuery("#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class] a").length != jQuery('.opt_Selected').length){
        console.log('Summary Changed');
        if (callback) {
            callback()
        }
        addAddSelectedClasses();
    }
    if (window.optCheckerLimit > 500) {
        window.optCheckerLimit = 0;
        clearInterval(optChecker);
    }
}

function addCheckOrderSummaryInterval () {
    console.log('Checking Summary');
    if (window.optChecker){
        clearInterval(optChecker);
    }
    window.optChecker = setInterval(function () {
        addCheckOrderSummary();
    },50);
}

function addRemoveAddedContainer() {
    jQuery('.opt_AddedContainer').remove();
}

function addAddedToCartHTML() {
    addRemoveAddedContainer();
    var html = '<div class="opt_AddedContainer"> <p>'+optLastName.name+' '+optLastName.price+' has been added to cart</p><div class="opt_Close"><p>X</p></div></div>'
    jQuery('.content-wrapper.container').prepend(html);
    jQuery('.opt_Close').click(function () {
        addRemoveAddedContainer();
    })
}

function addRemoveItemFunctionality() {
    jQuery("#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class] a:not(opt_RemoveAdded)").click(function () {
            addAddSelectedClasses();
            addCheckOrderSummaryInterval();
    }).addClass('opt_RemoveAdded');
}

function addAddtoCartFunctionality() {
    jQuery('.btn.btn-primary.btn-sm').each(function () {
        if (!jQuery(this).hasClass('opt_AddCartFunctionality')) {
            jQuery(this).click(function () {
                // Add the name to check against
                var name = jQuery(this).closest('.buy-check').find('.check-name').text().trim(),
                    price = jQuery(this).closest('.buy-check').find('.check-price li:eq(0)').text().trim().split('excl')[0].trim();
                window.optLastName.name = name;
                window.optLastName.price = price;
            });
        }
    });
}

function addOpenAreaFunction () {
    // Select the node that will be observed for mutations
    var targetNode = document.getElementById('ASPxPageControl1_C1');

    // Options for the observer (which mutations to observe)
    var config = { childList: true, subtree: true };

    // Callback function to execute when mutations are observed
    var callback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                console.log('Adding to Open Area')
                addAddtoCartFunctionality();
                addAddSelectedClasses();
            }
        }
    };

    // Create an observer instance linked to the callback function
    var observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);
}

addRemoveItemFunctionality();
addOpenAreaFunction();







window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
window.opta = jQuery('.order-processing').height();
window.optb = jQuery('.order-processing').offset().top;
window.optoff = opta-optsummaryHeight+optb;

function offset () {
    var scroll = jQuery(window).scrollTop();
    if (scroll <= optb + 20) {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
            "position": 'initial',
            "top": 'initial',
            "right": 'initial',
            "margin-top": "initial"
        });
    }else if (scroll > optb + 20 && optoff > scroll) {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
            "position": 'absolute',
            "top": scroll - optb,
            "right": '0px',
            "margin-top": "20px"
        });
        if (jQuery(window).width() < 1080 && jQuery(window).width() > 768) {
            jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
                "right":"20px"
            });
        } else {
            jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
                "right":"0px"
            });
        }
    } else if (scroll < optb + 20 && scroll < optoff) {
        jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
            "position": 'absolute',
            "top": opta-optsummaryHeight,
            "right": '0px',
            "margin-top": "20px"
        });
        if (jQuery(window).width() < 1080 && jQuery(window).width() > 768) {
            jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
                "right":"20px"
            });
        } else {
            jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').css({
                "right":"0px"
            });
        }
    }
}

jQuery(window).resize(function () {
    window.optsummaryHeight = jQuery('#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel').height();
    window.opta = jQuery('.order-processing').height();
    window.optb = jQuery('.order-processing').offset().top;
    window.optoff = opta-optsummaryHeight+optb;
})

jQuery(window).scroll(function () {
    offset();
})