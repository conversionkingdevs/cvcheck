function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
defer(function(){
  jQuery('body').addClass('opt01');
  var image = "//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/e4761c19bca23775afe0ce058b61a369_pic_+_blobs.png";
  var state = "";
  switch(window.location.pathname.split('-').pop()) {
    case "nsw":
        state = "New South Wales";
        break;
    case "qld":
        state = "Queensland";
        break;
    case "victoria":
        state = "Victoria";
        break;
    case "sa":
        state = "South Australia";
        break;
    case "wa":
        state = "Western Australia";
        break;
    case "nt":
        state = "Northern Territory";
        break;
    default:
      	break;   
  }

  var text = "Police Checks in "+state+" made easy";
 
  // Change Image
  jQuery('.page-hero span > img').attr('src', image);
  jQuery('.page-hero span > img').after(jQuery('.page-hero span > img').clone().addClass("opt-mobile").attr('src', '//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/2fa6b0b49c4c076cd960f9c861deee82_blob_mobile.png'));
  jQuery('.page-hero span > img:eq(0)').addClass("opt-desktop");
  // Change text
  jQuery('.page-hero .container').html('<div class="optContainer"><h1>'+text+'</h1></div>');
  // Add button
  jQuery('.page-hero .container h1').after('<div class="header-btn"><a href="/getstarted" class="get-started"><img src="/ResourcePackages/bootstrap/assets/dist/images/started-dots.png"> Get Started</a></div>');
}, ".page-hero");
