/* CUSTOM CODE */
console.clear();
$ = jQuery.noConflict();
$('body').addClass('opt03');
/**************************************
    LAYOUT
**************************************/
$('<p class="or">OR</p>').insertBefore($(".opt03 p:contains('Or complete')"));
$(".opt03 p:contains('Or complete')").text('Choose an account type below to get started');
$(".opt03 label[for='radioIndividual']").find('p').remove();
$(".opt03 label[for='radioIndividual'] .col-sm-11 img").wrap('<div class="icon" ></div>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .icon").append('<h3>Individual</h3>');
$(".opt03 label[for='radioIndividual'] .col-sm-11").append('<div class="text" ></div>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text").append('<h3>Individual</h3>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text").append('<ul ></ul>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text ul").append('<li>Fast Results</li>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text ul").append('<li>100% Online</li>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text ul").append('<li>Easy results sharing</li>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text ul").append('<li>Simplified Check Process</li>');
$(".opt03 label[for='radioIndividual'] .col-sm-11 .text").append('<a href="#" class="cta-button cta-button-primary">Get Started</a>');
$(".opt03 label[for='radioEmployer']").find('p').remove();
$(".opt03 label[for='radioEmployer'] .col-sm-11 img").wrap('<div class="icon" ></div>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .icon").append('<h3>Company</h3>');
$(".opt03 label[for='radioEmployer'] .col-sm-11").append('<div class="text" ></div>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text").append('<h3>Company</h3>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text").append('<ul ></ul>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text ul").append('<li>End-to-end screening requirement</li>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text ul").append('<li>Perform bulk checks</li>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text ul").append('<li>Full candidate management</li>');
$(".opt03 label[for='radioEmployer'] .col-sm-11 .text").append('<a href="#" class="cta-button cta-button-primary">Get Started</a>');
$(".opt03 #signupGetStarted").removeAttr('disabled');
$(".opt03 .signup .signup-form label.account-option a.cta-button").click(function(e){
   e.preventDefault();
   $this = $(this);
   $this.parents('.account-option').find('input').prop('checked','checked');
   gotoSignupPage(e);
});
/***** Adjust Height Function *****/
jQuery.fn.adjustOuterHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).outerHeight(true) > maxHeightFound ) {
            maxHeightFound = $(this).outerHeight(true);
        }
    });
    this.css('min-height',maxHeightFound);
};
$(".opt03 .signup .signup-form label.account-option .text ul").adjustOuterHeight();
$(window).resize(function(){
    $(".opt03 .signup .signup-form label.account-option .text ul").adjustOuterHeight();
});
