console.clear();
$ = jQuery.noConflict();
$('body').addClass('opt26-v2');


/**************************************
    LAYOUT V2
**************************************/ 
$(".opt26-v2 .page-content").removeClass('sfPublicWrapper').attr('id','home-container');


$("#home-container").html('');

var html = `
    <div id="home-slider-container">
        <div id="home-slider" class="flexslider">
            <ul class="slides">
                <li style="background: transparent url(https://i.imgur.com/cOKC6od.jpg) no-repeat center center"></li>
                <li style="background: transparent url(https://i.imgur.com/4rZRjZR.jpg) no-repeat center center"></li>
            </ul>
        </div>
        <div id="home-slider-content">
            <h1>The fastest, smartest and simplest way to verify information.</h1>
            <a href="https://cvcheck.com/nz/getstarted" class="button">Get started</a>
        </div>
    </div>
    <div id="links-bar-container">
        <div id="links-bar">
            <div class="link link-1">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible">Fast and Flexible</a>
            </div>
            <div class="link link-2">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications">Quick result notifications</a>
            </div>
            <div class="link link-3">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a>
            </div>
            <div class="link link-4">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/secure-and-trusted-results">Accredited, secure and trusted</a>
            </div>
        </div>
    </div>
    <div id="trusted-by-container">
        <h2>Trusted By</h2>
        <div id="trusted-by" class="flexslider">
            <ul class="slides">
                <li><img src="https://i.imgur.com/t9cSvPt.png" /></li>
                <li><img src="https://i.imgur.com/t9cSvPt.png" /></li>
                <li><img src="https://i.imgur.com/t9cSvPt.png" /></li>
                <li><img src="https://i.imgur.com/t9cSvPt.png" /></li>
                <li><img src="https://i.imgur.com/t9cSvPt.png" /></li>
            </ul>
        </div>
    </div>
    <div id="home-about-container">
        <div id="home-about">
            <div class="inner-wrap">
                <h2>More than 1000 checks across 190 countries</h2>
                <p>Wherever you are or wherever you’ve come from, CVCheck's got you covered.</p>
                <p>Whether you need an Australian or International Police Check, want to hire an overseas candidate, or are looking to verify your own international experience, we can provide the background checks you need.</p>
                <div class="links">
                    <div class="link link-1">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a>
                    </div>
                    <div class="link link-2">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/local-support-teams">Australian based, support team</a>
                    </div>
                    <div class="link link-3">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/protecting-your-privacy">Protecting your privacy</a>
                    </div>
                    <div class="link link-4">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/only-order-what-you-need">Only order what you need</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="home-boxes-container">
        <div id="home-boxes">
            <div class="box">
                <img src="https://i.imgur.com/Xr0JJcc.png" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/criminal-record-check">Criminal Record Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Criminal Record Check</li>
                        <li>Traffic Check</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/criminal-record-check" class="button">Find out more</a>
                </div>
            </div>
            <div class="box">
                <img src="https://i.imgur.com/g0tqbXr.png" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/employment-screening">Employment and Qualification Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Tertiary or trade qualifications</li>
                        <li>Reference checks</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/employment-screening" class="button">Find out more</a>
                </div>
            </div>
            <div class="box">
                <img src="https://i.imgur.com/Y9CpbDV.png" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/worldwide">International Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Overseas Police Checks</li>
                        <li>Verify Overseas Experience</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/worldwide" class="button">Find out more</a>
                </div>
            </div>
        </div>
    </div>
    <div id="home-testimonials-container">
        <h2>You're in good company...</h2>
        <div id="home-testimonials">
            <div class="testimonial">
                <h4><span>Speed and reliability are important to FBAA</span></h4>
                <blockquote>Speed and reliability are important to FBAA in processing member applications quickly and accurately. That’s why we’ve partnered with CVCheck as the preferred supplier of screening services to the finance industry.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Peter White,</strong> Chief Executive Officer</cite>
                    <small>Finance Brokers Association of Australia (FBAA)</small>
                    <img src="https://i.imgur.com/uijzo1L.png" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>I would highly recommend the service and easy-to-use software</span></h4>
                <blockquote>CVCheck has been a valuable addition to our recruitment process. It is fast, reliable, cost effective and easy to administrate. The client care provided by our Account Manager has been amazing. He is always available to help and no query is ever too hard for him to address for us. I would highly recommend the service and easy-to-use software.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Melissa Humphrey,</strong> Head of Human Resources </cite>
                    <small>Hugo Boss Australia Pty Ltd</small>
                    <img src="https://i.imgur.com/yWiHPS5.png" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>We often need to have staff checked and cleared to work within a matter of a few days</span></h4>
                <blockquote>CVCheck provides a centralised point where we can order and monitor all staff background checks. CVCheck has flexibility in its solutions, a fast turnaround, which is particularly useful for our events-driven business and remote locations where we often need to have staff checked and cleared to work within a matter of a few days.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Traci Eathorne,</strong> Executive Director Human Resources</cite>
                    <small>Delaware North</small>
                    <img src="https://i.imgur.com/0RddtOT.png" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>Compliance is key and CVCheck provides the tools we need to keep compliant – an invaluable resource</span></h4>
                <blockquote>With over 1000 staff and franchisees in our network at any given time, the efficiency of getting all necessary checks online using CVCheck, at the price they offer, means we are recruiting the right people for our businesses. Compliance is key, and CVCheck provides the tools we need to keep compliant – an invaluable resource.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Tracey Peverell,</strong> General Counsel</cite>
                    <small>Australian Couriers Pty Ltd trading as Fastway Couriers</small>
                    <img src="https://i.imgur.com/Ou3sQyg.png" />
                </div>
            </div>
        </div>
    </div>
    <div id="home-who-container">
        <div id="home-who">
            <h2>Who is CVCheck?</h2>
            <div class="who-boxes">
                <div class="who-box">
                    <img src="https://i.imgur.com/3tSjhni.png" />
                </div>
                <div class="who-box">
                    <img src="https://i.imgur.com/qZoYQfu.png" />
                </div>
                <div class="who-box">
                    <img src="https://i.imgur.com/jR5yYfu.png" />
                </div>
                <div class="who-box">
                    <img src="https://i.imgur.com/dA15uBO.png" />
                </div>
                <div class="who-box">
                    <img src="https://i.imgur.com/jMjnV03.png" />
                </div>
                <div class="who-box">
                    <img src="https://i.imgur.com/Qrc6U3j.png" />
                </div>
            </div>
        </div>
    </div>
    <div id="home-blog-container">
        <div class="top">
            <a href="https://checkpoint.cvcheck.com/" class="blog-title"><img src="https://i.imgur.com/wohr9Ov.png" /></a>
            <p>The smart way to get ahead for <a href="https://checkpoint.cvcheck.com/">HR Professionals</a>, hiring managers and recruiters. </p>
        </div>
        <div id="home-blog">
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/your-recruiting-for-retail-checklist/">
                    <img src="https://i.imgur.com/HMU5uXZ.jpg" />
                    <span class="title">Your recruiting-for-retail checklist</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/tink-recruitment-connecting-people-through-psychometrics/">
                    <img src="https://i.imgur.com/b5v59Pg.jpg" />
                    <span class="title">Tink Recruitment: Connecting people through psychometrics</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/a-career-protecting-the-community-meet-james-sutherland/">
                    <img src="https://i.imgur.com/YZzhLqL.jpg" />
                    <span class="title">A career protecting the community – meet James Sutherland</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
        </div>
    </div>
    <div id="home-why-container">
        <div id="home-why">
            <h2>Why choose CVCheck?</h2>
            <div class="columns">
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications" class="thumb"><img src="https://i.imgur.com/E0f6wxx.png" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications">Quick result notifications</a></h3>
                    <p>You can track your order in real time or just get on with your day and we'll notify you by email as soon as your check is completed. Then, download your results instantly from our website.</p>
                </div>
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork" class="thumb"><img src="https://i.imgur.com/bujvLo7.png" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a></h3>
                    <p>Want your candidates to take care of the admin? Simply choose the checks you want and we'll send a special code to your applicants. </p>
                </div>
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible" class="thumb"><img src="https://i.imgur.com/ko6ozN3.png" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible">Fast and flexible</a></h3>
                    <p>Enjoy fast online ordering and results that can be downloaded as soon as they're done so you never have to stand in a queue or wait for delivery by post.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="home-signup-container">
        <div id="home-signup">
            <h2>Sign up now - it's easy</h2>
            <p>Whether you are a small business, large enterprise or individual needing a check for a new employer, we have services and checks to suit you. Simply sign up to access our secure and easy-to-use online ordering system.</p>
            <a href="https://cvcheck.com/nz/getstarted" class="button">Get started</a>
        </div>
    </div>
`;

$("#home-container").append(html);




/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    /***** Home Container *****/
    #home-container {font-family: 'ChronicaPro-Regular';}
    
    
    /***** Home Slider *****/
    #home-slider-container {position: relative;}
    #home-slider {position: absolute; left: 0; top: 0; width: 100%; height: 100%; z-index: 1; padding: 0; margin: 0; border: none; border-radius: 0px;}
    #home-slider .slides {width: 100%; height: 100%; padding: 0; margin: 0; list-style: none; border: none; border-radius: 0px;}
    #home-slider .slides li {width: 100%; height: 100%; padding: 0; margin: 0; display: block; background-size: cover !important;}

    #home-slider-content {max-width: 1170px; margin: 0 auto; padding: 97px 15px 90px 15px; position: relative; z-index:10;}
    #home-slider-content h1 {font-weight: 700; font-family: 'ChronicaPro-Regular'; color:#007681; font-size:44px; line-height: 55px; padding: 0 0 26px 0; margin: 0; text-shadow: 0px 0px 2px rgba(0, 0, 0, 0.33); max-width: 540px;}
    #home-slider-content a.button {font-weight: 700; font-family: 'ChronicaPro-Regular'; display:inline-block; text-decoration: none; color: #ffffff; font-size: 16px; line-height: 16px; background-color: #007681; border-radius: 4px; padding: 13px 25px 13px 25px;}
    #home-slider-content a.button:hover {background-color: #54585a;}
    #home-slider-content a.button:after {display: inline-block; width:16px; height:16px; content:""; background:transparent url("https://i.imgur.com/2dsAWk4.png") no-repeat center center; background-size:16px auto; position:relative; top:3px; margin-left: 11px;}

    
    /***** Links Bar *****/
    #links-bar-container {background-color: #64abb1;}
    #links-bar {margin: 0 auto; padding: 0 15px 0 15px; text-align:center;}
    #links-bar:after {content:''; display:block; clear:both;}
    #links-bar .link {display:inline-block; margin:0 21px;}
    #links-bar .link:last-of-type {margin-right:0;}
    #links-bar .link a {display:block; text-decoration:none; color:#ffffff; font-size:16px; font-family: 'ChronicaPro-Bold'; padding:14px 15px 14px 58px;}
    #links-bar .link a:hover {background-color:#007681 !important;}
    #links-bar .link.link-1 a {background: transparent url("https://i.imgur.com/GkvwQfs.png") no-repeat 11px center; background-size: 35px auto;}
    #links-bar .link.link-2 a {background: transparent url("https://i.imgur.com/AnKrRAR.png") no-repeat 11px center; background-size: 35px auto;}
    #links-bar .link.link-3 a {background: transparent url("https://i.imgur.com/14yWrKS.png") no-repeat 11px center; background-size: 35px auto;}
    #links-bar .link.link-4 a {background: transparent url("https://i.imgur.com/0flRp6X.png") no-repeat 11px center; background-size: 35px auto;}


    /***** Trusted By *****/
    #trusted-by-container {background-color: #faf9f9; padding:25px 15px 60px 15px;}
    #trusted-by-container h2 {display:block; text-align:center; color:#54585A; font-size:34px; line-height:40px; padding:0 0 34px 0; margin:0; font-weight:400; font-family: 'ChronicaPro-Regular';}
    #trusted-by {max-width:1200px; margin: 0 auto; padding: 0 0 30px 0; text-align:center; background-color:transparent; border:none; border-bottom:1px solid #e0ebec; border-radius:0px;}
    #trusted-by .slides {padding:0; margin:0; border:none; border-radius:0px;}
    #trusted-by .slides li img {display:block; max-width:100%; height:auto; margin:0; width:auto; opacity:0.40;}
    #trusted-by .slides li img:hover {opacity:1;}


    /***** About *****/
    #home-about-container {background: #faf9f9; padding:0; background: #faf9f9 url("https://i.imgur.com/A1pohQk.jpg") no-repeat right top;}
    #home-about {max-width:1170px; margin:0 auto; padding:0 15px;}
    #home-about .inner-wrap {max-width:670px;}
    #home-about h2 {padding:3px 0 35px 0; margin:0; text-align:left; font-size:34px; line-height: 37px; color:#54585A; font-weight:400; font-family: 'ChronicaPro-Regular';}
    #home-about p {padding:0 0 10px 0; margin:0; text-align:left; font-size:14px; line-height: 20px; color:#54585A; font-weight:400; font-family: 'ChronicaPro-Regular';}
    #home-about .links:after {content:""; display:block; clear:both;}
    #home-about .links .link {display:block; float:left; width:47.5%; margin-right:5%; padding:8px 0 12px 0;}
    #home-about .links .link:nth-child(2n) {margin-right:0;}
    #home-about .links .link:nth-child(2n+1) {clear:left;}
    #home-about .links .link a {display:inline-block; text-decoration:none; color:#54585A; font-family: 'ChronicaPro-Regular'; font-weight:700; color:#54585A; font-size:14px; line-height:20px; padding:14px 0 14px 59px; background-size: auto 38px !important;}
    #home-about .links .link a:hover {color:#007681;}
    #home-about .links .link.link-1 a {background:transparent url("https://i.imgur.com/YVO7TKp.png") no-repeat left center;}
    #home-about .links .link.link-2 a {background:transparent url("https://i.imgur.com/wxtO2pf.png") no-repeat left center;}
    #home-about .links .link.link-3 a {background:transparent url("https://i.imgur.com/9iJAJeX.png") no-repeat left center;}
    #home-about .links .link.link-4 a {background:transparent url("https://i.imgur.com/dokAoG0.png") no-repeat left center;}


    /***** Boxes *****/
    #home-boxes-container {background: #faf9f9; padding:60px 0 0 0; border-bottom:63px solid #f0f0f0;}
    #home-boxes {max-width:1170px; margin:0 auto -63px auto; padding:0 15px;}
    #home-boxes .box {float:left; width:32.46%; margin:0 1.31% 0 0; position:relative; border-radius:9px; overflow:hidden; -webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3);}
    #home-boxes .box:hover {-webkit-box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7); -moz-box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7); box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7);}
    #home-boxes .box:nth-child(3n) {margin-right:0;}
    #home-boxes:after {content:""; display:block; clear:both;}
    #home-boxes img {display:block; position:absolute; left:0; top:0; width:100%; height:100%; object-fit:cover; z-index:1;}
    #home-boxes .inner-box {position:relative; z-index:10; padding:71px 23px 86px 23px; text-align:center;}
    #home-boxes h3 {font-size:24px; line-height:34px; font-weight:normal; font-family: 'ChronicaPro-Bold'; padding:0; max-width:274px; margin:0 auto;}
    #home-boxes h3 a {text-decoration:none; color:#ffffff; font-weight:normal; font-family: 'ChronicaPro-Bold';}
    #home-boxes h3 a:hover {color:rgba(255,255,255,0.75);}
    #home-boxes hr {display:block; border:none; width:100%; height:2px; margin:20px 0 17px 0; padding:0; background-color:rgba(255,255,255,0.6);}
    #home-boxes ul {padding:0; margin:0; list-style:none;}
    #home-boxes ul li {padding:0 0 19px 0; margin:0 0 5px 0; list-style:none; display:block; font-size:16px; line-height:18px; color:#ffffff; background: transparent url("https://i.imgur.com/XXiu4Nd.png") no-repeat center bottom; background-size:12px auto;}
    #home-boxes ul li:last-child {padding-bottom:0; margin-bottom:0; background-image:none;}
    #home-boxes .button {text-decoration:none; display:inline-block; background-color:#ffffff; color:#007681; font-size:14px; line-height:16px; padding:11px 40px 11px 40px; border-radius:3px; text-align:left; margin-top:19px;}
    #home-boxes .button:after {display: inline-block; width:16px; height:16px; content:""; background:transparent url("https://i.imgur.com/HUCwXrV.png") no-repeat center center; background-size:16px auto; position:relative; top:3px; margin-left: 13px;}
    #home-boxes .button:hover {background-color: #64abb1; color:#ffffff;}
    #home-boxes .button:hover:after {-webkit-filter: brightness(0) invert(1); -moz-filter: brightness(0) invert(1); filter: brightness(0) invert(1);}


    /***** Testimonials *****/
    #home-testimonials-container {background: #f0f0f0; padding:56px 0 36px 0;}
    #home-testimonials-container h2 {display:block; text-align:center; color:#54585A; font-size:34px; line-height:40px; padding:0 0 45px 0; margin:0; font-weight:400; font-family: 'ChronicaPro-Regular';}
    #home-testimonials {max-width:1170px; margin:0 auto 0 auto; padding:0 15px;}
    #home-testimonials:after {content:""; display:block; clear:both;}
    #home-testimonials .testimonial {float:left; width:48.95%; margin:0 2.1% 24px 0; background-color:#ffffff; position:relative; border-radius:9px; overflow:hidden;  -webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); padding:12px 20px 12px 20px; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
    #home-testimonials .testimonial:after {content:""; display:block; width:100%; height:12px; position:absolute; left:0; bottom:0; background: rgb(209,230,232); background: -moz-linear-gradient(left, rgba(209,230,232,1) 0%, rgba(102,173,179,1) 100%); background: -webkit-linear-gradient(left, rgba(209,230,232,1) 0%,rgba(102,173,179,1) 100%); background: linear-gradient(to right, rgba(209,230,232,1) 0%,rgba(102,173,179,1) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d1e6e8', endColorstr='#66adb3',GradientType=1 );}
    #home-testimonials .testimonial:nth-child(2n) {margin-right:0;}
    #home-testimonials .testimonial:nth-child(2n+1) {clear:left;}
    #home-testimonials h4 {font-weight:normal; margin:0; font-size:16px; line-height: 22px; color:#007681; font-style:italic; font-family: 'ChronicaPro-Bold'; padding:0 0 0 71px; background: transparent url("https://i.imgur.com/bFQkVfM.png") no-repeat left center; background-size:51px auto;}
    #home-testimonials h4 span {display:block;}
    #home-testimonials blockquote {display:block; padding:0; margin:16px 0 0 0; font-weight:normal; font-style:normal; border:none; font-size:14px; line-height:20px; font-family: 'ChronicaPro-Regular'; color:#54585A;}
    #home-testimonials cite {display:block; padding:0; margin:0 0 0 0; font-weight:normal; font-style:normal; border:none; font-size:14px; line-height:20px; font-family: 'ChronicaPro-Regular'; color:#007681;}
    #home-testimonials cite strong {font-weight:normal; font-family: 'ChronicaPro-Bold';}
    #home-testimonials small {display:block; padding:0; margin:0 0 0 0; font-weight:normal; font-style:normal; border:none; font-size:14px; line-height:20px; font-family: 'ChronicaPro-Regular'; color:#54585A;}
    #home-testimonials .testimonial-info {position:relative; padding:16px 115px 16px 0;}
    #home-testimonials .testimonial-info img {display:block; position:absolute; right:0; top:50%; transform:translate(0,-50%); max-width:100%; width:auto; height:auto; margin:0; opacity:0.6;}


    /***** Who *****/
    #home-who-container {background: #faf9f9; padding:0 0 0 0; border-top:42px solid #f0f0f0;}
    #home-who {max-width:1140px; margin:-42px auto 0 auto; padding:44px 70px 14px 70px; background: transparent url("https://i.imgur.com/53LDnuQ.png") no-repeat left top; background-size:100% 100%;}
    #home-who h2 {font-size:34px; line-height: 44px; font-weight:700; font-family: 'ChronicaPro-Regular'; padding:0 0 41px 0; margin:0; text-align:left; color:#ffffff;}
    #home-who .who-boxes .who-box {float:left; width:28%; margin:0 8% 37px 0;}
    #home-who .who-boxes .who-box:nth-child(3n) {margin-right:0;}
    #home-who .who-boxes .who-box:nth-child(3n+1) {clear:left;}
    #home-who .who-boxes .who-box img {display:block; max-width:100%; width:auto; height:auto; max-height:70px; margin:0;}
    #home-who .who-boxes:after {content:""; display:block; clear:both;}


    /***** Blog *****/
    #home-blog-container {background: #faf9f9; padding:60px 0 60px 0;}

    #home-blog-container .top {text-align:center; padding:0 15px 35px 15px;}
    #home-blog-container .blog-title {display:inline-block;}
    #home-blog-container .blog-title img {display:block; max-width:100%; height:auto; margin:0;}
    #home-blog-container .top p {padding:14px 0 0 0; margin:0; max-width:none; font-size:16px; line-height:20px; color:#54585A; font-family: 'ChronicaPro-Regular';}
    #home-blog-container .top p a {color:#007681; text-decoration:none;}
    #home-blog-container .top p a:hover {text-decoration:underline;}

    #home-blog {max-width:1170px; margin:0 auto 0 auto; padding:0 15px;}
    #home-blog .box {float:left; width:32.46%; margin:0 1.31% 0 0; position:relative; border-radius:9px; overflow:hidden; -webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); -moz-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.3); text-decoration:none;}
    #home-blog .box:hover {-webkit-box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7); -moz-box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7); box-shadow: 0px 0px 14px 0px rgba(0, 0, 0, 0.7);}
    #home-blog .box a {display:block;}
    #home-blog .box a:after {content:""; display:block; width:275px; height:275px; border-radius:50%; background-color:#007681; position:absolute; right:-67px; bottom:-84px; opacity:0.4; z-index:1;}
    #home-blog .box a:hover:after {width:1000px; height:1000px; right:-200px; bottom:-200px;}
    #home-blog .box:nth-child(3n) {margin-right:0;}
    #home-blog .box a img {display:block; max-width:100%; height:auto; width:auto;}
    #home-blog .box a span {display:block;}
    #home-blog .box span.title {font-size:24px; line-height:30px; font-family: 'ChronicaPro-Bold'; color:#ffffff; padding:0 25px; position:absolute; left:0; bottom:79px; box-sizing:border-box; width:100%; z-index:10;}
    #home-blog .box span.button {font-size:16px; line-height:20px; font-family: 'ChronicaPro-Bold'; color:#ffffff; position:absolute; right:20px; bottom:22px; box-sizing:border-box; text-align:right; border-left:2px solid #ffffff; padding:4px 0 4px 14px; z-index:10;}
    #home-blog .box span.button:after {display: inline-block; width:16px; height:16px; content:""; background:transparent url("https://i.imgur.com/2dsAWk4.png") no-repeat center center; background-size:16px auto; position:relative; top:3px; margin-left: 15px;}
    #home-blog:after {content:""; display:block; clear:both;}


    /***** Why *****/
    #home-why-container {background: #f0f0f0; padding:54px 0 59px 0;}
    #home-why {max-width:1170px; margin:0 auto 0 auto; padding:0 15px;}
    #home-why h2 {font-size:34px; line-height: 44px; font-weight:400; font-family: 'ChronicaPro-Regular'; padding:0 0 45px 0; margin:0; text-align:center; color:#54585A;}
    #home-why .columns:after {content:""; display:block; clear:both;}
    #home-why .columns .column {float:left; width:31.58%; margin:0 2.63% 0 0}
    #home-why .columns .column:nth-child(3n) {margin-right:0;}
    #home-why a.thumb {display:inline-block;}
    #home-why a.thumb:hover img {opacity:0.75;}
    #home-why img {display:block; max-width:100%; height:auto; max-height:61px; margin:0;}
    #home-why h3 {font-size:16px; line-height: 20px; font-weight:700; font-family: 'ChronicaPro-Regular'; padding:16px 0 8px 0; margin:0; text-align:left; color:#54585A;}
    #home-why h3 a {text-decoration:none; color:#54585A;}
    #home-why h3 a:hover {color:#007681;}
    #home-why p {font-size:14px; line-height: 20px; font-weight:400; font-family: 'ChronicaPro-Regular'; padding:0 0 0 0; margin:0; text-align:left; color:#54585A; max-width:none;}


    /***** Sign Up *****/
    #home-signup-container {background: transparent url("https://i.imgur.com/ogGVuf3.jpg") no-repeat center top; background-size:cover; padding:50px 15px 60px 15px;}
    #home-signup {max-width:670px; margin:0 auto 0 auto; padding:0; text-align:center;}
    #home-signup h2 {color:#FFFFFF; font-size:34px; line-height:44px; font-weight:normal; font-family: 'ChronicaPro-Bold'; padding:0 0 35px 0; margin:0; text-align:center;}
    #home-signup p {color:#FFFFFF; font-size:16px; line-height:20px; font-weight:normal; font-family: 'ChronicaPro-Regular'; padding:0; margin:0; text-align:center; max-width:none;}
    #home-signup a.button {font-weight: 400; font-family: 'ChronicaPro-Regular'; display:inline-block; text-decoration: none; color: #007681; font-size: 14px; line-height: 16px; background-color: #ffffff; border-radius: 4px; padding: 11px 33px 11px 33px; margin:21px 0 0 0;}
    #home-signup a.button:after {display: inline-block; width:16px; height:16px; content:""; background:transparent url("https://i.imgur.com/HUCwXrV.png") no-repeat center center; background-size:16px auto; position:relative; top:3px; margin-left: 14px;}
    #home-signup .button:hover {background-color: #54585a; color:#ffffff;}
    #home-signup .button:hover:after {-webkit-filter: brightness(0) invert(1); -moz-filter: brightness(0) invert(1); filter: brightness(0) invert(1);}


    /***** Media Queries *****/
    @media screen and (max-width: 1500px){
        /***** Home Slider *****/
        #home-slider-content {padding: 77px 15px 70px 15px;}
        #home-slider-content h1 {font-size:40px; line-height: 50px;  max-width: 540px;}
    }
    
    @media screen and (max-width: 1180px){
        /***** Links Bar *****/
        #links-bar {text-align:justify; font-size:0; line-height:0;}
        #links-bar .link {margin:0 15px 0 0; display:inline-block;}
        #links-bar:after {content:""; display:inline-block; width:100%; height:0;}
        #links-bar .link:last-of-type {margin-right:0;}
        #links-bar .link a {font-size:14px; padding:14px 15px 14px 40px; line-height: 20px;}
        #links-bar .link.link-1 a {background: transparent url("https://i.imgur.com/GkvwQfs.png") no-repeat left center; background-size: 30px auto;}
        #links-bar .link.link-2 a {background: transparent url("https://i.imgur.com/AnKrRAR.png") no-repeat left center; background-size: 30px auto;}
        #links-bar .link.link-3 a {background: transparent url("https://i.imgur.com/14yWrKS.png") no-repeat left center; background-size: 30px auto;}
        #links-bar .link.link-4 a {background: transparent url("https://i.imgur.com/0flRp6X.png") no-repeat left center; background-size: 30px auto;}
        
        /***** Trusted By *****/
        #trusted-by-container {padding:25px 15px 50px 15px;}

        /***** About *****/
        #home-about-container {padding:30px 0 15px 0; background-size:auto 100%;}

        /***** Boxes *****/
        #home-boxes-container {padding:50px 0 0 0;}
        #home-boxes .inner-box {padding:50px 15px 60px 15px;}

        /***** Testimonials *****/
        #home-testimonials-container {padding:56px 0 26px 0;}

        /***** Who *****/
        #home-who {padding:44px 25px 14px 25px;}
        #home-who .who-boxes .who-box {width:25%; margin:0 10% 37px 0;}
      
        /***** Blog *****/
        #home-blog-container {padding:50px 0 50px 0;}
        #home-blog .box span.title {font-size:22px; line-height:28px; padding:0 15px;}
        #home-blog .box span.button {right:15px; bottom:20px;}
        #home-blog .box a:after {right:-77px; bottom:-94px;}

        /***** Why *****/
        #home-why-container {padding:44px 0 49px 0;}

        /***** Sign Up *****/
        #home-signup-container {padding:40px 15px 50px 15px;}
    }


    @media screen and (max-width: 960px){
        /***** Home Slider *****/
        #home-slider-content h1 {font-size:36px; line-height: 46px;}

        /***** Links Bar *****/
        #links-bar .link {width:25%; margin:0; vertical-align:top;}
        #links-bar .link a {font-size:12px; padding:14px 15px 14px 40px; line-height: 20px; text-align:left;}
        #links-bar .link.link-1 a {max-width:140px;}
        
        /***** About *****/
        #home-about-container {background-position:240% center !important;}
        #home-about .inner-wrap {max-width:500px;}
        #home-about .links {max-width:400px;}
        #home-about .links .link {width:47.5%; margin-right:5%; padding:4px 0 6px 0;}

        /***** Boxes *****/
        #home-boxes .inner-box {padding:30px 15px 35px 15px;}
        #home-boxes h3 {font-size:20px; line-height:30px;}
        #home-boxes hr {margin:10px 0 12px 0;}

        /***** Testimonials *****/
        #home-testimonials .testimonial {padding:12px 15px 12px 15px;}
        #home-testimonials .testimonial-info {padding-right:90px;}
        #home-testimonials .testimonial-info img {max-width:80px;}
        #home-testimonials h4 {font-size:15px; line-height: 21px; padding:0 0 0 61px; background-size:44px auto;}

        /***** Who *****/
        #home-who {padding:44px 25px 14px 25px;}
        #home-who .who-boxes .who-box {width:33.333%; margin:0 0 25px 0 !important;}
        #home-who .who-boxes .who-box img {width:80%; margin:0 auto;}
        #home-who .who-boxes .who-box:nth-child(3n+1) {clear:left;}
        #home-who .who-boxes .who-box:nth-child(2n+1) {clear:none;}
      
        /***** Blog *****/
        #home-blog-container {padding:50px 0 50px 0;}
        #home-blog .box span.title {font-size:18px; line-height:24px; padding:0 15px; bottom:70px;}
        #home-blog .box span.button {right:15px; bottom:15px; font-size:14px; line-height: 18px;}
        #home-blog .box a:after {right:-97px; bottom:-114px;}
    }


    @media screen and (max-width: 767px){
        /***** Links Bar *****/
        #links-bar {padding:0; margin:0; background-color:transparent; border:none; border-radius:0px;}
        #links-bar ul.slides {padding:0; margin:0; background-color:transparent; border:none; border-radius:0px;}
        #links-bar ul.slides li {padding:0; margin:0;}
        #links-bar ul.slides li .link {float:none; display:block; text-align:center; width:auto; margin:0;}
        #links-bar ul.slides li .link a {float:none; display:inline-block; text-align:center; width:auto; margin:0; max-width:none; font-size:16px; background-size:40px auto !important; padding:15px 0 15px 51px; background-color:transparent !important;}

        /***** Home Slider *****/
        #home-slider .slides li {background-position:left center !important;}
        #home-slider-content {text-align:center; padding:35px 0 30px 0; max-width:290px; margin:0 auto;}
        #home-slider-content h1 {font-size:30px; line-height: 32px; text-align:center;}
        #home-slider-content a.button {display:block; max-width:240px; margin:0 auto; padding-top:11px; padding-bottom:12px;}

        /***** Trusted By *****/
        #trusted-by-container {padding-bottom:30px;}
        #trusted-by-container h2 {font-size:30px; line-height: 30px; padding-bottom:24px;}
        #trusted-by {padding-bottom:0; border-bottom:none;}
        #trusted-by .slides li img {opacity:0.2;} 
        #trusted-by .slides li.active-slides img {opacity:1;}

        /***** About *****/
        #home-about-container {background-position:100px center !important; padding:11px 0 9px 0;}
        #home-about .inner-wrap {max-width:none;}
        #home-about .links {display:none;}
        #home-about h2 {font-size:30px; line-height: 37px; padding:0 0 21px 0;}
        #home-about p {padding-bottom:9px;}

        /***** Boxes *****/
        #home-boxes-container {padding-top:30px;}
        #home-boxes {padding:0;}
        #home-boxes .box {float:none; width:auto; margin:0 0 4px 0; box-shadow:none; border-radius:0px;}
        #home-boxes .inner-box {padding:12px 15px 18px 15px;}
        #home-boxes h3 {font-size:20px; line-height:28px; max-width:240px;}
        #home-boxes hr {display:none;}
        #home-boxes ul {display:none;}
        #home-boxes .button {margin-top:18px; display:block; max-width:202px; margin-left:auto; margin-right:auto; padding-left:0; padding-right:0; text-align:center;}

        /***** Testimonials *****/
        #home-testimonials-container {padding:19px 0 42px 0; background-color:#faf9f9;}
        #home-testimonials-container h2 {font-size:30px; line-height:37px; padding:0 0 7px 0; max-width:240px; margin:0 auto;}
        #home-testimonials.flexslider {background:transparent; padding:0 0; margin:0; border-radius:0px; border:none;}
        #home-testimonials.flexslider .slides {padding:0; margin:0; list-style:none; border:none; border-radius:0px; background-color:transparent;}
        #home-testimonials.flexslider .slides li {padding:0;}
        #home-testimonials .testimonial {float:none; width:auto; margin-right:0; padding-bottom:21px; padding-top:15px; margin:15px;}
        #home-testimonials .testimonial .testimonial-info {padding-right:0; padding-bottom:39px; min-height: 1px !important;}
        #home-testimonials .testimonial .testimonial-info img {right:0; top:auto; bottom:0; transform:none; max-width:100%; width:auto;}
        #home-testimonials h4 {font-size:16px; line-height: 22px; padding-left:0; background:transparent url("https://i.imgur.com/ifmxjVe.png") no-repeat left center; background-size:54px auto !important; min-height:1px !important;}
        #home-testimonials h4 span {padding-top:0 !important;}
        #home-testimonials blockquote {margin-top:13px; min-height: 1px !important; margin-bottom:8px;}
        #home-testimonials .flex-control-nav {padding:0; margin:0; bottom:-17px; width:200px; left:50%; margin-left:-100px; text-align:center; box-sizing: border-box;}
        #home-testimonials .flex-control-nav li {padding:0; margin:0 8px;}
        #home-testimonials .flex-control-nav li a {-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width:16px; height:16px; border:1px solid #007681; background-color:#ffffff; box-shadow:none;}
        #home-testimonials .flex-control-nav li a.flex-active {background-color:#007681;}

        /***** Who *****/
        #home-who {padding:25px 15px 74px 15px;}
        #home-who h2 {font-size:30px; line-height: 44px; text-align:center; padding-bottom:20px;}
        #home-who .who-boxes {background:transparent; padding:0 0; margin:0; border-radius:0px; border:none;}
        #home-who .who-boxes .slides {background:transparent; padding:0 0; margin:0; border-radius:0px; border:none;}
        #home-who .who-boxes .slides .who-box {padding:0; margin:0 auto 19px auto !important; float:none; width:auto; max-width:217px;}
        #home-who .who-boxes .slides .who-box img {max-width:100%; height:auto; width:auto; margin:0;}
        #home-who .flex-control-nav {padding:0; margin:0; bottom:-48px; width:200px; left:50%; margin-left:-100px; text-align:center; box-sizing: border-box;}
        #home-who .flex-control-nav li {padding:0; margin:0 8px;}
        #home-who .flex-control-nav li a {-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width:16px; height:16px; border:1px solid #ffffff; background-color:transparent; box-shadow:none;}
        #home-who .flex-control-nav li a.flex-active {background-color:#ffffff;}

        /***** Why *****/
        #home-why-container {padding:24px 0 0 0; background-color:#faf9f9;}
        #home-why {padding:0 15px 0 15px;}
        #home-why h2 {font-size:30px; line-height: 37px; max-width:280px; margin:0 auto; text-align:center; padding-bottom:22px;}
        #home-why .columns .column {float:none; width:auto; margin:0 0 29px 0;}
        #home-why img {max-width:40px; max-height:none;}
        #home-why h3 {padding:6px 0 8px 0;}

        /***** Blog *****/
        #home-blog-container {background: #f0f0f0; padding:30px 0 20px 0;}
        #home-blog-container .top {padding:0 15px 17px 15px; max-width:300px; margin:0 auto;}
        #home-blog-container .blog-title {display:block; max-width:202px; margin:0 auto;}
        #home-blog-container .top p {padding:21px 0 0 0;}
        #home-blog .box {float:none; width:auto; margin:0 0 10px 0;}
        #home-blog .box:nth-child(3) {display: none;}
        #home-blog .box a:after {width:271; height:271px; right:-66px; bottom:-113px;}
        #home-blog .box a img {max-height:260px; max-width:none; width:100%; object-fit: cover;}
        #home-blog .box span.title {font-size:18px; line-height:24px; bottom:88px; max-width:90%;}
        #home-blog .box span.button {font-size:14px; line-height:16px; right:15px; bottom:25px; padding:2px 0 2px 13px;}

        /***** Sign Up *****/
        #home-signup-container {background-position:75% top !important; padding:32px 15px 40px 15px;}
        #home-signup h2 {font-size:34px; line-height:37px; padding:0 0 27px 0; max-width:280px; margin:0 auto;}
        #home-signup p {font-size:16px; line-height:20px; max-width:280px; margin:0 auto;}
        #home-signup a.button {display:block; max-width:240px; margin:21px auto 0 auto; text-align:center; padding-left:5px; padding-right:5px;}
    }
</style>
`);




/**************************************
    LOAD EXTERNAL SCRIPTS
**************************************/
function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};


/** Flexslider **/
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" />')
getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js",start_flexslider);

function start_flexslider(){
    $("#home-slider").flexslider({
        directionNav: false,
        controlNav: false
    });

    $("#trusted-by").flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 240,
        itemMargin: 0,
        directionNav: false,
        controlNav: false,
        move: 1,
        minItems: 1,
        maxItems: 5,
        start: function (slider) {
            window.addCurrentSlidesClass(slider);
        },
        after: function (slider) {
            window.addCurrentSlidesClass(slider);
        }
    });

    if($(window).width() < 767){
        $("#home-testimonials").addClass('flexslider');
        $("#home-testimonials").wrapInner('<ul class="slides" />');
        $("#home-testimonials .testimonial").wrap('<li />');
        $("#home-testimonials").flexslider({
            directionNav: false,
            slideshow: false,
            smoothHeight: true,
            animation: "slide"
        });


        $("#links-bar").addClass('flexslider');
        $("#links-bar").wrapInner('<ul class="slides" />');
        $("#links-bar .link").wrap('<li />');
        $("#links-bar").flexslider({
            directionNav: false,
            controlNav: false,
            slideshow: true,
            smoothHeight: false,
            animation: "slide"
        });
        $("#links-bar a").click(function(e){
            e.preventDefault();
        });


        $(".who-boxes").addClass('flexslider');
        $(".who-boxes").wrapInner('<ul class="slides" />');
        var divs = $(".who-boxes .slides > div");
        for(var i = 0; i < divs.length; i+=3) {
            divs.slice(i, i+3).wrapAll("<li />");
        };
        $(".who-boxes").flexslider({
            directionNav: false,
            controlNav: true,
            slideshow: true,
            smoothHeight: false,
            animation: "slide"
        });


        $("#home-blog-container").insertAfter($("#home-why-container"));
    };
};

window.addCurrentSlidesClass = function (slider) {
    $('#trusted-by .flexslider li').removeClass("active-slides");
    var startli = (slider.move * (slider.currentSlide));
    var endli = (slider.move * (slider.currentSlide + 1));
    for (i = startli + 1; i <= endli; i++) {
        $('.flexslider li:nth-child(' + i + ')').addClass('active-slides');
    };
};

/***** Adjust Height Function *****/
jQuery.fn.adjustHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).height() > maxHeightFound ) {
            maxHeightFound = $(this).height();
        }
    });
    this.css('min-height',maxHeightFound);
};

jQuery.fn.adjustOuterHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).outerHeight(true) > maxHeightFound ) {
            maxHeightFound = $(this).outerHeight(true);
        }
    });
    this.css('min-height',maxHeightFound);
};


$("#home-container").css('padding-top',$('header').outerHeight(true));
$("#home-boxes h3").adjustOuterHeight();
$("#home-boxes ul").adjustOuterHeight();
$("#home-testimonials h4").adjustOuterHeight();
$("#home-testimonials .testimonial").each(function(){
    $this = $(this);
    $this.find('h4 span').css('padding-top',($this.find('h4').outerHeight(true) - $this.find('h4 span').height()) / 2);
});
$("#home-testimonials blockquote").adjustHeight();
$("#home-testimonials .testimonial-info").adjustOuterHeight();
$("#home-blog .box span.title").adjustOuterHeight();

var currentWidth = jQuery(window).width();
$(window).resize(function(){
    $("#home-container").css('padding-top',$('header').outerHeight(true));
    $("#home-boxes h3").adjustOuterHeight();
    $("#home-boxes ul").adjustOuterHeight();
    $("#home-testimonials h4").adjustOuterHeight();
    $("#home-testimonials .testimonial").each(function(){
        $this = $(this);
        $this.find('h4 span').css('padding-top',($this.find('h4').outerHeight(true) - $this.find('h4 span').height()) / 2);
    });
    $("#home-testimonials blockquote").adjustHeight();
    $("#home-testimonials .testimonial-info").adjustOuterHeight();
    $("#home-blog .box span.title").adjustOuterHeight();

    
    if (currentWidth <= 767) {
        if (jQuery(window).width() > 767) {
            window.location.href = window.location.href;
        }
    } else if (currentWidth > 767) {
        if (jQuery(window).width() <= 767) {
            window.location.href = window.location.href;
        }
    }

});