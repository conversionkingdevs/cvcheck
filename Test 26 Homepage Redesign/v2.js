$ = jQuery.noConflict();
$('body').addClass('opt26-v2');
/**************************************
    LAYOUT V2
**************************************/ 
$(".opt26-v2 .page-content").removeClass('sfPublicWrapper').attr('id','home-container');
$("#home-container").html('');
var html = `
    <div id="home-slider-container">
        <div id="home-slider" class="flexslider">
            <ul class="slides">
                <li style="background: transparent url(//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7613ac47f556611858a33bab6c33eac0_cokc6od.jpg) no-repeat center center"></li>
                <li style="background: transparent url(//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/4b05dba9b460c961ac767809f4cacbb4_4rzrjzr.jpg) no-repeat center center"></li>
            </ul>
        </div>
        <div id="home-slider-content">
            <h1>The fastest, smartest and simplest way to verify information.</h1>
            <a href="https://cvcheck.com/nz/getstarted" class="button">Get started</a>
        </div>
    </div>
    <div id="links-bar-container">
        <div id="links-bar">
            <div class="link link-1">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible">Fast and Flexible</a>
            </div>
            <div class="link link-2">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications">Quick result notifications</a>
            </div>
            <div class="link link-3">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a>
            </div>
            <div class="link link-4">
                <a href="https://cvcheck.com/nz/cvcheckfeatures/secure-and-trusted-results">Accredited, secure and trusted</a>
            </div>
        </div>
    </div>
    <div id="trusted-by-container">
        <h2>Trusted By</h2>
        <div id="trusted-by" class="flexslider">
            <ul class="slides">
                <li><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9797dd6d78a959d414464fecaa017418_t9csvpt.jpg" /></li>
                <li><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9797dd6d78a959d414464fecaa017418_t9csvpt.jpg" /></li>
                <li><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9797dd6d78a959d414464fecaa017418_t9csvpt.jpg" /></li>
                <li><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9797dd6d78a959d414464fecaa017418_t9csvpt.jpg" /></li>
                <li><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9797dd6d78a959d414464fecaa017418_t9csvpt.jpg" /></li>
            </ul>
        </div>
    </div>
    <div id="home-about-container">
        <div id="home-about">
            <div class="inner-wrap">
                <h2>More than 1000 checks across 190 countries</h2>
                <p>Wherever you are or wherever you’ve come from, CVCheck's got you covered.</p>
                <p>Whether you need an Australian or International Police Check, want to hire an overseas candidate, or are looking to verify your own international experience, we can provide the background checks you need.</p>
                <div class="links">
                    <div class="link link-1">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a>
                    </div>
                    <div class="link link-2">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/local-support-teams">Australian based, support team</a>
                    </div>
                    <div class="link link-3">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/protecting-your-privacy">Protecting your privacy</a>
                    </div>
                    <div class="link link-4">
                        <a href="https://cvcheck.com/nz/cvcheckfeatures/only-order-what-you-need">Only order what you need</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="home-boxes-container">
        <div id="home-boxes">
            <div class="box">
                <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/11d047b5652b22cb21aac20a5cde591b_xr0jjcc.jpg" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/criminal-record-check">Criminal Record Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Criminal Record Check</li>
                        <li>Traffic Check</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/criminal-record-check" class="button">Find out more</a>
                </div>
            </div>
            <div class="box">
                <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/74df1793b2cd3129685ed0117b8de70e_g0tqbxr.jpg" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/employment-screening">Employment & Qualification Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Employment Reference CHeck</li>
                        <li>Employment Verification</li>
                        <li>Qualification Check</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/employment-screening" class="button">Find out more</a>
                </div>
            </div>
            <div class="box">
                <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/9c59e6a96f7fa834ec3a5e94d88f7d4f_y9cpbdv.jpg" />
                <div class="inner-box">
                    <h3><a href="https://cvcheck.com/nz/worldwide">Children's Worker Safety Checks</a></h3>
                    <hr />
                    <ul>
                        <li>Trusted by Ministry of Health and Education, other government departments and employers.</li>
                    </ul>
                    <a href="https://cvcheck.com/nz/worldwide" class="button">Find out more</a>
                </div>
            </div>
        </div>
    </div>
    <div id="home-testimonials-container">
        <h2>You're in good company...</h2>
        <div id="home-testimonials">
            <div class="testimonial">
                <h4><span>Speed and reliability are important to FBAA</span></h4>
                <blockquote>Speed and reliability are important to FBAA in processing member applications quickly and accurately. That’s why we’ve partnered with CVCheck as the preferred supplier of screening services to the finance industry.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Peter White,</strong> Chief Executive Officer</cite>
                    <small>Finance Brokers Association of Australia (FBAA)</small>
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/a5ce133b5150747b4dac7a02037633f0_uijzo1l.jpg" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>I would highly recommend the service and easy-to-use software</span></h4>
                <blockquote>CVCheck has been a valuable addition to our recruitment process. It is fast, reliable, cost effective and easy to administrate. The client care provided by our Account Manager has been amazing. He is always available to help and no query is ever too hard for him to address for us. I would highly recommend the service and easy-to-use software.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Melissa Humphrey,</strong> Head of Human Resources </cite>
                    <small>Hugo Boss Australia Pty Ltd</small>
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/977bb140cf9c1a3121c8f0a12847d667_ywihps5.png" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>We often need to have staff checked and cleared to work within a matter of a few days</span></h4>
                <blockquote>CVCheck provides a centralised point where we can order and monitor all staff background checks. CVCheck has flexibility in its solutions, a fast turnaround, which is particularly useful for our events-driven business and remote locations where we often need to have staff checked and cleared to work within a matter of a few days.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Traci Eathorne,</strong> Executive Director Human Resources</cite>
                    <small>Delaware North</small>
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/bfd5852a949f1d33914722a48f7e96c0_0rddtot.jpg" />
                </div>
            </div>
            <div class="testimonial">
                <h4><span>Compliance is key and CVCheck provides the tools we need to keep compliant – an invaluable resource</span></h4>
                <blockquote>With over 1000 staff and franchisees in our network at any given time, the efficiency of getting all necessary checks online using CVCheck, at the price they offer, means we are recruiting the right people for our businesses. Compliance is key, and CVCheck provides the tools we need to keep compliant – an invaluable resource.</blockquote>
                <div class="testimonial-info">
                    <cite><strong>Tracey Peverell,</strong> General Counsel</cite>
                    <small>Australian Couriers Pty Ltd trading as Fastway Couriers</small>
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/676d06def973cbcef2be8895f1424fe3_ou3sqyg.jpg" />
                </div>
            </div>
        </div>
    </div>
    <div id="home-who-container">
        <div id="home-who">
            <h2>Who is CVCheck?</h2>
            <div class="who-boxes">
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/6ffa3de5997f823e6a2f0851f7c2c3bb_asx.png" />
                </div>
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/4c13b3fd6986edb11fe830c0d7982091_offices_in_aus.png" />
                </div>
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/7c8ebb37280dc736d8f672be4735f7d0_businesses.png" />
                </div>
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/c1f5cadf12d0c50e503021f7de6a225f_checks_per_year.png" />
                </div>
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/4ea896b4a0e1812391c41d6c59937cd2_years_experience.png" />
                </div>
                <div class="who-box">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/791b4866c02dc92ea5435426bf28e4a5_countries.png" />
                </div>
            </div>
        </div>
    </div>
    <div id="home-blog-container">
        <div class="top">
            <a href="https://checkpoint.cvcheck.com/" class="blog-title"><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/0aa6470ea653946ee6f7b47ec63e1d26_wohr9ov.jpg" /></a>
            <p>The smart way to get ahead for <a href="https://checkpoint.cvcheck.com/">HR Professionals</a>, hiring managers and recruiters. </p>
        </div>
        <div id="home-blog">
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/your-recruiting-for-retail-checklist/">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/4238021a86a809a57fcd1fe8310cb466_hmu5uxz.jpg" />
                    <span class="title">Your recruiting-for-retail checklist</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/tink-recruitment-connecting-people-through-psychometrics/">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/8b0897f66cb21679809a5b422a85e582_b5v59pg.jpg" />
                    <span class="title">Tink Recruitment: Connecting people through psychometrics</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
            <div class="box">
                <a href="https://checkpoint.cvcheck.com/a-career-protecting-the-community-meet-james-sutherland/">
                    <img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/1b09224d7b9f3c411bdec748c4eb2f72_yzzhlql.jpg" />
                    <span class="title">A career protecting the community – meet James Sutherland</span>
                    <span class="button">Read Article</span>
                </a>
            </div>
        </div>
    </div>
    <div id="home-why-container">
        <div id="home-why">
            <h2>Why choose CVCheck?</h2>
            <div class="columns">
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications" class="thumb"><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/49a077540b5f6c4e9c162260e7a93d27_e0f6wxx.jpg" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/quick-result-notifications">Quick result notifications</a></h3>
                    <p>You can track your order in real time or just get on with your day and we'll notify you by email as soon as your check is completed. Then, download your results instantly from our website.</p>
                </div>
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork" class="thumb"><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/a60b03b9e4dfdd49010e6f1a2c6398fe_bujvlo7.jpg" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/reduce-your-paperwork">Reduce your paperwork</a></h3>
                    <p>Want your candidates to take care of the admin? Simply choose the checks you want and we'll send a special code to your applicants. </p>
                </div>
                <div class="column">
                    <a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible" class="thumb"><img src="//useruploads.visualwebsiteoptimizer.com/useruploads/335128/images/18e024f015cd3ba014e44c35b700908a_ko6ozn3.jpg" /></a>
                    <h3><a href="https://cvcheck.com/nz/cvcheckfeatures/fast-and-flexible">Fast and flexible</a></h3>
                    <p>Enjoy fast online ordering and results that can be downloaded as soon as they're done so you never have to stand in a queue or wait for delivery by post.</p>
                </div>
            </div>
        </div>
    </div>
    <div id="home-signup-container">
        <div id="home-signup">
            <h2>Sign up now - it's easy</h2>
            <p>Whether you are a small business, large enterprise or individual needing a check for a new employer, we have services and checks to suit you. Simply sign up to access our secure and easy-to-use online ordering system.</p>
            <a href="https://cvcheck.com/nz/getstarted" class="button">Get started</a>
        </div>
    </div>
`;
$("#home-container").append(html);
/**************************************
    LOAD EXTERNAL SCRIPTS
**************************************/
function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};
/** Flexslider **/
$('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/flexslider.min.css" />')
getScript("https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.4/jquery.flexslider-min.js",start_flexslider);
function start_flexslider(){
    $("#home-slider").flexslider({
        directionNav: false,
        controlNav: false
    });
    if ($(window).width() >= 767) {
        $("#trusted-by").flexslider({
            animation: "slide",
            animationLoop: false,
            itemWidth: 240,
            itemMargin: 0,
            directionNav: false,
            controlNav: false,
            move: 1,
            minItems: 1,
            maxItems: 5,
            start: function (slider) {
                window.addCurrentSlidesClass(slider);
            },
            after: function (slider) {
                window.addCurrentSlidesClass(slider);
            }
        });
        }
    if($(window).width() < 767){
        $("#trusted-by").flexslider({
            directionNav: false,
            controlNav: false,
            slideshow: false,
            smoothHeight: true,
            animation: "slide"
        });
        $("#home-testimonials").addClass('flexslider');
        $("#home-testimonials").wrapInner('<ul class="slides" ></ul>');
        $("#home-testimonials .testimonial").wrap('<li ></li>');
        $("#home-testimonials").flexslider({
            directionNav: false,
            slideshow: false,
            smoothHeight: true,
            animation: "slide"
        });
        $("#links-bar").addClass('flexslider');
        $("#links-bar").wrapInner('<ul class="slides" ></ul>');
        $("#links-bar .link").wrap('<li ></li>');
        $("#links-bar").flexslider({
            directionNav: false,
            controlNav: false,
            slideshow: true,
            smoothHeight: false,
            animation: "slide"
        });
        $("#links-bar a").click(function(e){
            e.preventDefault();
        });
        $(".who-boxes").addClass('flexslider');
        $(".who-boxes").wrapInner('<ul class="slides" ></ul>');
        var divs = $(".who-boxes .slides > div");
        for(var i = 0; i < divs.length; i+=3) {
            divs.slice(i, i+3).wrapAll("<li ></li>");
        };
        $(".who-boxes").flexslider({
            directionNav: false,
            controlNav: true,
            slideshow: true,
            smoothHeight: false,
            animation: "slide"
        });
        $("#home-blog-container").insertAfter($("#home-why-container"));
    };
};
window.addCurrentSlidesClass = function (slider) {
    $('#trusted-by .flexslider li').removeClass("active-slides");
    var startli = (slider.move * (slider.currentSlide));
    var endli = (slider.move * (slider.currentSlide + 1));
    for (i = startli + 1; i <= endli; i++) {
        $('.flexslider li:nth-child(' + i + ')').addClass('active-slides');
    };
};
/***** Adjust Height Function *****/
jQuery.fn.adjustHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).height() > maxHeightFound ) {
            maxHeightFound = $(this).height();
        }
    });
    this.css('min-height',maxHeightFound);
};
jQuery.fn.adjustOuterHeight = function() {
    var maxHeightFound = 0;
    this.css('min-height','1px');
    this.each(function() {
        if( $(this).outerHeight(true) > maxHeightFound ) {
            maxHeightFound = $(this).outerHeight(true);
        }
    });
    this.css('min-height',maxHeightFound);
};
$("#home-container").css('padding-top',$('header').outerHeight(true));
$("#home-boxes h3").adjustOuterHeight();
$("#home-boxes ul").adjustOuterHeight();
$("#home-testimonials h4").adjustOuterHeight();
$("#home-testimonials .testimonial").each(function(){
    $this = $(this);
    $this.find('h4 span').css('padding-top',($this.find('h4').outerHeight(true) - $this.find('h4 span').height()) / 2);
});
$("#home-testimonials blockquote").adjustHeight();
$("#home-testimonials .testimonial-info").adjustOuterHeight();
$("#home-blog .box span.title").adjustOuterHeight();
var currentWidth = jQuery(window).width();
$(window).resize(function(){
    $("#home-container").css('padding-top',$('header').outerHeight(true));
    $("#home-boxes h3").adjustOuterHeight();
    $("#home-boxes ul").adjustOuterHeight();
    $("#home-testimonials h4").adjustOuterHeight();
    $("#home-testimonials .testimonial").each(function(){
        $this = $(this);
        $this.find('h4 span').css('padding-top',($this.find('h4').outerHeight(true) - $this.find('h4 span').height()) / 2);
    });
    $("#home-testimonials blockquote").adjustHeight();
    $("#home-testimonials .testimonial-info").adjustOuterHeight();
    $("#home-blog .box span.title").adjustOuterHeight();
    
    if (currentWidth <= 767) {
        if (jQuery(window).width() > 767) {
            window.location.href = window.location.href;
        }
    } else if (currentWidth > 767) {
        if (jQuery(window).width() <= 767) {
            window.location.href = window.location.href;
        }
    }
});
