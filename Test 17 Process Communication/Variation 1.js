
function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {

jQuery('body').addClass('test17v1');

    var containerBefore 	= jQuery('.page-content .container.video-background');

    var content 			= `<div class="container tabs">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">How it works:</h1>
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#check_on_myself" aria-controls="check_on_myself" role="tab" data-toggle="tab">Check on myself</a></li>
                        <li role="presentation"><a href="#checks_on_employees" aria-controls="checks_on_employees" role="tab" data-toggle="tab">Checks on employees</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane text-center active" id="check_on_myself">
                            `;


    // first pane content				
    content 			+= `<div class="row inner-content">
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Sign up & login to your CVCheck account.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Select your <br/>check.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Add your details.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Review & pay.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Upload your ID.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>Email <br/>notification once <br/>complete.</p>
        </div>
    </div>`;

    // button
    content 		+= `<div class="row"><div class="col-md-12 text-center"><button class="btn btn-outline">Get Started</button></div></div>`; 


    content 			+= `</div>
                        <div role="tabpanel" class="tab-pane text-center" id="checks_on_employees">`;

    // second pane content				
    content 			+= `<div class="row inner-content">
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Sign up & login to your CVCheck account.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Select your <br/>check.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Add your details.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Review & pay.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Upload your ID.</p>
        </div>
        <div class="col-md-2 text-center">
            <img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt="" class="img-responsive" />
            <p>2 Email <br/>notification once <br/>complete.</p>
        </div>
    </div>`;


    // button
    content 		+= `<div class="row"><div class="col-md-12 text-center"><button class="btn btn-outline">Get Started</button></div></div>`;



    content 		+= `</div>
                    </div>

                </div>
            </div>
        </div>
    </div>`;

    jQuery(content).insertBefore(containerBefore);

}, '.page-content .container.video-background')