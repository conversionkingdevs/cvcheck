function createHTML () {
    var html = ''
        html += '<div class="opt-container" id="opt-Resume">';
        html +=     '<p class="opt-title">Jump back in:</p>';
        html +=     '<div class="opt-content">';
        html +=         '<div class="opt-col" id="opt-orderstarted">';
        html +=             '<p class="opt-title opt-smaller opt-grey">Order Started</p>';
        html +=             '<p class="opt-title opt-small"></p>';
        html +=         '</div>';
        html +=         '<div class="opt-col" id="opt-ordertype">';
        html +=                 '<p class="opt-title opt-smaller opt-grey">Order Type</p>';
        html +=                 '<p class="opt-title opt-small"></p>';
        html +=         '</div>';
        html +=         '<div class="opt-col" id="opt-applicantname">';
        html +=                 '<p class="opt-title opt-smaller opt-grey">Applicant Name</p>';
        html +=                 '<p class="opt-title opt-small"></p>';
        html +=         '</div>';
        html +=         '<div class="opt-col" id="opt-button">';
        html +=                 '<a class="btn btn-primary" href="#">Begin Order<i class="fa fa-chevron-circle-right"></i></a>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';

    return html;    
}

function populate () {
    var el = jQuery('#ctl00_MainContent_ASPxPageControlEmployerTabs_ChecksAwaitingOrderControl1_ASPxGridViewCartOrders_DXMainTable tr:last-of-type ul'),
        link = el.find('.button > a').attr('href'),
        started = el.find('li:nth-of-type(2) .cOrderNeedingAttentionRowValue').text(),
        type = el.find('li:nth-of-type(3) .cOrderNeedingAttentionRowValue').text(),
        name = el.find('li:nth-of-type(5) .cOrderNeedingAttentionRowValue').text();

        // update text and attr

        jQuery('#opt-orderstarted .opt-small').text(started);
        jQuery('#opt-ordertype .opt-small').text(type);
        jQuery('#opt-applicantname .opt-small').text(name);
        jQuery('#opt-button').attr('dest', link);

        jQuery('#opt-button a').click(function (e) { 
            e.preventDefault();
            eval(jQuery(this).parent().attr('dest'));
        });

}

function start () {
    jQuery('body').addClass('opt-30-v1');
    jQuery('.employer-dashboard .lower-panel').before(createHTML());
    populate();
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(start, '#ctl00_MainContent_ASPxPageControlEmployerTabs_ChecksAwaitingOrderControl1_ASPxGridViewCartOrders_DXMainTable tr:last-of-type ul');

 