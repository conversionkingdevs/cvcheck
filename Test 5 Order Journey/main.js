
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) === 0) return c.substring(name.length,c.length);
    }
    return "";
}

var setCrossDomainCookie = function(name, value, days) {
    var expires;
    if (days >= 0) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" +
        date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/;domain=cvcheck.com;";
};

function deleteCookie( name, path, domain ) {
    if( getCookie( name ) ) {
      document.cookie = name + "=" +
        ((path) ? ";path="+path:"")+
        ((domain)?";domain="+domain:"") +
        ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }
}

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

var url = window.location.pathname.split("/");
url = url[url.length-1]
setCrossDomainCookie('optFirstTime','true');
switch(url){
    case "SignupComplete.aspx":
        setCrossDomainCookie('optFirstTime','true');
        break;
    case "Home.aspx":
        var loaded = getCookie('optFirstTime');
        if(loaded != ""){
            if(loaded == "true"){
              
                setTimeout(function(){
                    if(jQuery("#ctl00_MainContent_ButtonIndividualOrder").length > 0){
                        window.location.href = '/members/CartSelectChecksPackages.aspx?mode=IndividualCheckOrder';
                    } else {
                        window.location.href = '/members/CartSelectChecksPackages.aspx?mode=EmployerInitial';
                    }
                }, 500); 
            }
        }
        break;
    case "getstarted":
        //if there is a value in the cookie display it.
        var loaded = getCookie('optCode');
        if(loaded != ""){
            var data = JSON.parse(loaded);
            var name = data["name"];
            var className = ((name.length > 50 ) ? "opt-long" : "opt-short");
            jQuery(".opt-item-container").remove();
            jQuery("h1").after('<div class="opt-item-container '+className+'"></div>');
            jQuery(".opt-item-container").append('<div></div><span>'+name+'</span>');
        }
        jQuery("h1").text("Create An Account");
        
        break;
    case "CartSelectChecksPackages.aspx":
        var loaded = getCookie('optCode');
        if(loaded != ""){
            var data = JSON.parse(loaded);
            ShoppingCartAddCheck(data["code"]);

            var name = data["name"];
            var code =  data["code"];

            var className = ((name.length > 50 ) ? "opt-long" : "opt-short");

            jQuery(".opt-package-container").remove();
            jQuery(".instructions").after('<div class="opt-package-container"></div>');
            jQuery(".opt-package-container").append("<span>We've added the following check to your cart:</span>");
            jQuery(".opt-package-container").append('<div class="opt-item-container '+className+'"></div>');
            jQuery(".opt-item-container").append('<span>'+name+'</span>');
            jQuery(".opt-item-container").append('<section class="opt-button-container"><button type="submit" name="ctl00$MainContent$ShoppingCart$ShoppingCartCallbackPanel$ShoppingCartContinueButton" value="" id="ShoppingCartContinueButton" class="aspNetDisabled btn btn-primary" native="true" encodehtml="False">Save &amp; Continue<i class="fa fa-chevron-circle-right"></i></button><span class="opt-remove">Remove <img src="/images/delete_icon.png" alt="delete"></span></section>');
            jQuery(".opt-package-container").append("<span>Add another Check</span>");

            jQuery(".opt-remove").click(function(){
                jQuery("#ctl00_MainContent_ShoppingCart_ShoppingCartCallbackPanel_ASPxGridViewCartContents_DXMainTable tr[class] a").each(function(){
                    var next = jQuery(this).parent().next();
                    console.log(next.text().trim(), name, next.text().trim() == name);
                    if(next.text().trim() == name) {
                        jQuery(this).click();
                    }
                }); 
                jQuery(".opt-package-container").remove();
            });
            
            deleteCookie('optCode','/',"cvcheck.com");
            deleteCookie('optFirstTime','/',"cvcheck.com");
        }
        break;
    default:
        //wait for custom buttons
        defer(function(){
            jQuery('.cta-button[href="http://ww1.cvcheck.com/public/SignUpNow.aspx"]').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var name = jQuery('.cta-buttons-container .cta-button[href="/getstarted"]:eq(0)').parents(".cta-buttons").find("h4").text();
                var code = jQuery('.cta-buttons-container .cta-button[href="/getstarted"]:eq(0)').attr("data-target");
                if(name != null && code != null) {
                    setCrossDomainCookie('optCode','{"code":'+code+',"name":"'+name+'"}');
                }
                window.location.href = jQuery(this).attr("href");
            });
        }, '.cta-button[href="http://ww1.cvcheck.com/public/SignUpNow.aspx"]');

        //main button
        defer(function(){
            jQuery('.cta-buttons-container .cta-button[href="/getstarted"] , .cta-buttons-container .cta-button[href="/nz/getstarted"]').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var name = jQuery(this).parents(".cta-buttons").find("h4").text();
                var code = jQuery(this).attr("data-target");
                if(name != null && code != null) {
                    setCrossDomainCookie('optCode','{"code":'+code+',"name":"'+name+'"}');
                }
                window.location.href = jQuery(this).attr("href");
            });
        }, '.cta-buttons-container .cta-button[href="/getstarted"], .cta-buttons-container .cta-button[href="/nz/getstarted"]');

        //pdp button
        defer(function(){
            jQuery('.information > div > a.get-started:eq(0)').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var name = jQuery(".information h3:eq(0)").text().trim();
                var code = jQuery('.information > div > a.get-started:eq(0)').attr("data-target");
                if(name != null && code != null) {
                    setCrossDomainCookie('optCode','{"code":'+code+',"name":"'+name+'"}');
                }
                window.location.href = jQuery(this).attr("href");
            });
        }, '.information > div > a.get-started');

        defer(function(){
            jQuery('a[data-mobile-target]').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                var name = jQuery(this).children().find("h4").text().trim();
                var code = jQuery(this).attr("data-mobile-target");
                if(name != null && code != null) {
                    setCrossDomainCookie('optCode','{"code":'+code+',"name":"'+name+'"}');
                }
                window.location.href = jQuery(this).attr("href");
            });
        }, 'a[data-mobile-target]');
        
        break;
}