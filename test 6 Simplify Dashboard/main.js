function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
var data1 = [
    {
        "title": "<h3 class='opt-white'>Choose Your Checks</h3>",
        "content": '<ul><li><div class="opt-icon"><img src="https://cvcheck.com/images/default-source/default-album/06_privacy_guaranteed-01.png?sfvrsn=84d47b5d_0" alt=""></div><div><span>Protecting your privacy</span></div></li><li><div class="opt-icon"><img src="https://cvcheck.com/images/default-source/default-album/smlfastflexible.jpg?sfvrsn=6989745d_0" alt=""></div><div><span>Fast and flexible</span></div></li><li><div class="opt-icon"><img src="https://cvcheck.com/images/default-source/default-album/smlaccredited.jpg?sfvrsn=2389745d_0" alt=""></div><div><span>Accredited, secure and trusted</span></div></li></ul>',
        "link":'<a href="/members/CartSelectChecksPackages.aspx?mode=EmployerInitial&action=new" class="opt-white"><span>Start your Order</span> <i class="fa fa-chevron-circle-right"></i></a>'
    },
    {
        "title": "<h3 class='opt-white'>Learn More</h3>",
        "content": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>",
        "link":'<a href="#" class="opt-white"><span>Learn More</span> <i class="fa fa-chevron-circle-right"></i></a>'
    },
    {
        "title": "<h3 class='opt-white'>Invite Users</h3>",
        "content": '<p>Invitees will appear as a user for your organisation as soon as they have completed the online Sign Up process (they will not be activated until we have received a clear, readable copy of their photo ID).</p>',
        "link":'<a href="/sam/SAMCreateUser.aspx" class="opt-white"><span>Invite Users</span> <i class="fa fa-chevron-circle-right"></i></a>'
    }
];
var data2 = {
    "title" : "<h3>Popular Checks</h3>",
    "data" : [
        {
            "content" : '<div class="buy-check" ><p class="check-name">Australia: National Police Check</p><ul class="check-price"><li> $45.00 <span class="inc-gst">excl GST</span></li><li id="ASPxGridViewCurrentCountryCategories_dxdt0_ASPxDataView1_0_IT0_0_GSTPrice_0"> $49.50 <span class="inc-gst">incl GST</span></li></ul><p class="check-time"> Estimated delivery time: 1 day *</p><div class="buttons"> <a class="check-more-info">FIND OUT MORE</a> <button type="button" data-value="536" class="btn btn-primary btn-sm"> ADD TO ORDER<i class="fa fa-chevron-circle-right"></i></button></div></div>',
            "popup" : '<div id="ctl00_MainContent_ASPxPopupControlCheckDetail_PW-1" class="dxpcLite_CVCheckClean dxpclW" popup="" style="height: 496px; width: 684px; cursor: default; z-index: 12000; visibility: visible; position: absolute; left: 168px; top: 44px; opacity: 1; display: table;"><div class="dxpc-mainDiv dxpc-shadow" style="opacity:1;"><div class="dxpc-header dxpc-withBtn" style="cursor:move;-khtml-user-select:none;" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1"><div class="dxpc-closeBtn" id="ctl00_MainContent_ASPxPopupControlCheckDetail_HCB-1"> <img class="dxWeb_pcCloseButton_CVCheckClean" src="/DXR.axd?r=1_19-kx2Dg" alt="[Close]"></div><div class="dxpc-headerContent" style="padding-left: 0px !important; padding-right: 24px !important; line-height: 12px; height: 16px; vertical-align: baseline;"> <span class="dxpc-headerText dx-vam" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1T">Check Detail</span></div><b class="dx-clear"></b></div><div class="dxpc-contentWrapper" style="display: table; height: 464px;"><div class="dxpc-content" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWC-1" style="display: block; height: 430px;"><iframe id="ctl00_MainContent_ASPxPopupControlCheckDetail_CIF-1" scrolling="auto" frameborder="0" src="https://ww1.cvcheck.com/members/popups/PopupShowCheckDetail.aspx?checkId=536" style="width: 100%; height: 430px; overflow: auto; backface-visibility: hidden; vertical-align: text-bottom; visibility: visible;"></iframe></div></div></div></div>'
        },
        {
            "content" : '<div class="buy-check" ><p class="check-name"> Australia: Traffic - QLD</p><ul class="check-price"><li> $43.00 <span class="inc-gst">excl GST</span></li><li id="ASPxGridViewCurrentCountryCategories_dxdt1_ASPxDataView1_1_IT2_1_GSTPrice_2"> $47.30 <span class="inc-gst">incl GST</span></li></ul><p class="check-time"> Estimated delivery time: 10 days *</p><div class="buttons"> <a class="check-more-info">FIND OUT MORE</a> <button type="button" data-value="44" class="btn btn-primary btn-sm"> ADD TO ORDER<i class="fa fa-chevron-circle-right"></i> </button></div></div>',
            "popup" : '<div id="ctl00_MainContent_ASPxPopupControlCheckDetail_PW-1" class="dxpcLite_CVCheckClean dxpclW" popup="" style="height: 496px; width: 692px; cursor: default; z-index: 12000; visibility: visible; display: table; position: absolute; left: 168px; top: 44px; opacity: 1;"><div class="dxpc-mainDiv dxpc-shadow" style="opacity:1;"><div class="dxpc-header dxpc-withBtn" style="cursor:move;-khtml-user-select:none;" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1"><div class="dxpc-closeBtn" id="ctl00_MainContent_ASPxPopupControlCheckDetail_HCB-1"> <img class="dxWeb_pcCloseButton_CVCheckClean" src="/DXR.axd?r=1_19-kx2Dg" alt="[Close]"></div><div class="dxpc-headerContent" style="padding-left: 0px !important; padding-right: 24px !important; line-height: 12px; height: 16px; vertical-align: baseline;"> <span class="dxpc-headerText dx-vam" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1T">Check Detail</span></div><b class="dx-clear"></b></div><div class="dxpc-contentWrapper" style="display: table; height: 464px;"><div class="dxpc-content" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWC-1" style="display: block; height: 430px;"><iframe id="ctl00_MainContent_ASPxPopupControlCheckDetail_CIF-1" scrolling="auto" frameborder="0" src="/members/popups/PopupShowCheckDetail.aspx?checkId=44" style="width: 100%; height: 430px; overflow: auto; backface-visibility: hidden; vertical-align: text-bottom;"></iframe></div></div></div></div>'
        },
        {
            "content" : '<div class="buy-check" data-value="182"><p class="check-name"> Australia: Anti Money Laundering</p><ul class="check-price"><li> $35.00 <span class="inc-gst">excl GST</span></li><li id="ASPxGridViewCurrentCountryCategories_dxdt2_ASPxDataView1_2_IT0_2_GSTPrice_0"> $38.50 <span class="inc-gst">incl GST</span></li></ul><p class="check-time"> Estimated delivery time: 1 day *</p><div class="buttons"> <a class="check-more-info">FIND OUT MORE</a> <button type="button"  data-value="182" class="btn btn-primary btn-sm"> ADD TO ORDER<i class="fa fa-chevron-circle-right"></i> </button></div></div>',
            "popup" :'<div id="ctl00_MainContent_ASPxPopupControlCheckDetail_PW-1" class="dxpcLite_CVCheckClean dxpclW" popup="" style="height: 496px; width: 692px; cursor: default; z-index: 12000; visibility: visible; display: table; position: absolute; left: 168px; top: 44px; opacity: 1;"><div class="dxpc-mainDiv dxpc-shadow" style="opacity:1;"><div class="dxpc-header dxpc-withBtn" style="cursor:move;-khtml-user-select:none;" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1"><div class="dxpc-closeBtn" id="ctl00_MainContent_ASPxPopupControlCheckDetail_HCB-1"> <img class="dxWeb_pcCloseButton_CVCheckClean" src="/DXR.axd?r=1_19-kx2Dg" alt="[Close]"></div><div class="dxpc-headerContent" style="padding-left: 0px !important; padding-right: 24px !important; line-height: 12px; height: 16px; vertical-align: baseline;"> <span class="dxpc-headerText dx-vam" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWH-1T">Check Detail</span></div><b class="dx-clear"></b></div><div class="dxpc-contentWrapper" style="display: table; height: 464px;"><div class="dxpc-content" id="ctl00_MainContent_ASPxPopupControlCheckDetail_PWC-1" style="display: block; height: 430px;"><iframe id="ctl00_MainContent_ASPxPopupControlCheckDetail_CIF-1" scrolling="auto" frameborder="0" src="/members/popups/PopupShowCheckDetail.aspx?checkId=182" style="width: 100%; height: 430px; overflow: auto; backface-visibility: hidden; vertical-align: text-bottom;"></iframe></div></div></div></div>'
        },
    ]
}
//members portal code
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
if(window.location.pathname == "/members/CartSelectChecksPackages.aspx"){
    var value = getURLParameter("select")
    if(value){
        ShoppingCartCallbackPanel.PerformCallback('AddCheck,' + parseInt(value));
    }
} else {
    defer(function(){
        var orderResults = parseInt(jQuery("#ctl00_MainContent_ASPxCallbackPanelEmployerStats_LabelEmployerStatsOrderResults").text().trim());
        var orderAttention = parseInt(jQuery("#ctl00_MainContent_ASPxCallbackPanelEmployerStats_LabelEmployerStatsAttention").text().trim());
        var unredeemedCodes = parseInt(jQuery("#ctl00_MainContent_ASPxCallbackPanelEmployerStats_LabelEmployerStatsPrepaidOutstanding").text().trim());
        var ordersCart = parseInt(jQuery("#ctl00_MainContent_ASPxCallbackPanelEmployerStats_LabelEmployerStatsCart").text().trim());

        //check if first time using
        if((orderResults + orderAttention + unredeemedCodes + ordersCart) == 0) {
            jQuery("body").addClass("opt6");
            var variation = 1;
            switch(variation){
                case 0:
                    jQuery("body").addClass("opt6v1");
                    jQuery(".opt-container").remove()
                    jQuery(".employer-dashboard").append('<div class="opt-container"></div>');
                    for(var i = 0; i < data1.length; i++){
                        jQuery(".opt-container").append(' <div class="opt-item"></div>');
                        jQuery(".opt-item:eq("+i+")").append('<div class="opt-header">'+data1[i]["title"]+'</div>');
                        jQuery(".opt-item:eq("+i+")").append('<div class="opt-content">'+data1[i]["content"]+'</div>');
                        jQuery(".opt-item:eq("+i+")").append('<div class="opt-link">'+data1[i]["link"]+'</div>');
                    }
                    break;
                case 1: 
                    jQuery("body").addClass("opt6v2");
                    jQuery(".opt-container").remove()
                    jQuery(".employer-dashboard").append('<div class="opt-container"></div>');

                    jQuery(".opt-container").append(data2["title"]);

                    var data = data2["data"]

                    for(var i = 0; i < data.length; i++){
                        jQuery(".opt-container").append(' <div class="opt-item"></div>');
                        jQuery(".opt-item:eq("+i+")").append('<div class="opt-content">'+data[i]["content"]+'</div>');
                        jQuery(".opt-item:eq("+i+")").append('<div class="opt-popup"><div class="opt-overlay"></div>'+data[i]["popup"]+'</div>');
                    }
                    jQuery(".dxpc-closeBtn, .opt-overlay").click(function(){
                        jQuery(".opt-popup").removeClass("opt-active");
                    });
                    
                    jQuery(".check-more-info").click(function(){
                        jQuery(".opt-popup").removeClass(".opt-active");
                        jQuery(this).parents(".opt-item").find(".opt-popup").addClass("opt-active");
                    });
                    jQuery(".buy-check button").click(function(){
                        window.location.href = "/members/CartSelectChecksPackages.aspx?mode=EmployerInitial&select=" + jQuery(this).attr("data-value");
                    });
                break;
            }
        }
    }, ".employer-dashboard");
}